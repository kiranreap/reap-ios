//
//  ReapWebObject.h
//  Reap
//
//  Created by Kiran Patchigolla on 11/7/12.
//  Copyright (c) 2012 Kiran Patchigolla. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyCLController.h"
#import <AWSiOSSDK/S3/AmazonS3Client.h>
#import "Constants.h"
@protocol ReapWebDelegate
-(void)changeTabs:(int)index;
@end
@interface ReapWebObject : NSObject <UIWebViewDelegate,MyCLControllerDelegate>
+(ReapWebObject*)sharedWebView;
@property (nonatomic, retain) UIWebView *webView;
@property (nonatomic, assign) id <ReapWebDelegate> reapWebDelegate;
@property (nonatomic, retain) MyCLController *locationController;
-(void) navigateToHome;
@property (nonatomic, retain) AmazonS3Client *s3;

@end
