//
//  SocialmarksViewController.m
//  Reap
//
//  Created by Kiran Patchigolla on 11/1/12.
//  Copyright (c) 2012 Kiran Patchigolla. All rights reserved.
//

#import "SocialmarksViewController.h"
#import "ReapWebObject.h"
@interface SocialmarksViewController ()

@end

@implementation SocialmarksViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
                    self.tabBarItem.image = [UIImage imageNamed:@"stream"];        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    UIWebView *webView = [[ReapWebObject sharedWebView] webView];
    webView.frame = self.view.frame;
    [webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"Reap.navigate('%@',true)", @"explore"]];
    
    [self.view addSubview:webView];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
