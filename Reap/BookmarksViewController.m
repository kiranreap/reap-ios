//
//  BookmarksViewController.m
//  Reap
//
//  Created by Kiran Patchigolla on 11/1/12.
//  Copyright (c) 2012 Kiran Patchigolla. All rights reserved.
//

#import "BookmarksViewController.h"
#import "ReapWebObject.h"
@interface BookmarksViewController ()

@end

@implementation BookmarksViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
            self.tabBarItem.image = [UIImage imageNamed:@"user"];        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    locationController = [[MyCLController alloc] init];
    ReapWebObject *reapWebObject = [ReapWebObject sharedWebView];
    locationController.delegate1 =reapWebObject;
    reapWebObject.locationController = locationController;
    [locationController.locationManager startUpdatingLocation];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    ReapWebObject *reapWebObject = [ReapWebObject sharedWebView];
    UIWebView *webView = [reapWebObject webView];
    webView.frame = self.view.frame;
    [webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"Reap.navigate('%@',true)", @"activity"]];
    [self.view addSubview:webView];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
