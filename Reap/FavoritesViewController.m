//
//  FavoritesViewController.m
//  Reap
//
//  Created by Kiran Patchigolla on 11/1/12.
//  Copyright (c) 2012 Kiran Patchigolla. All rights reserved.
//

#import "FavoritesViewController.h"
#import "ReapWebObject.h"
@interface FavoritesViewController ()

@end

@implementation FavoritesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
                    self.tabBarItem.image = [UIImage imageNamed:@"mydocs"];        
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    UIWebView *webView = [[ReapWebObject sharedWebView] webView];
    webView.frame = self.view.frame;
    [webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"Reap.navigate('%@',true)", @"mysnaps"]];
    
    [self.view addSubview:webView];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
