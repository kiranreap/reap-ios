//
//  AppDelegate.m
//  Reap
//
//  Created by Kiran Patchigolla on 11/1/12.
//  Copyright (c) 2012 Kiran Patchigolla. All rights reserved.
//

#import "AppDelegate.h"


#import "BookmarksViewController.h"
#import "SocialmarksViewController.h"
#import "CameraViewController.h"
#import "FavoritesViewController.h"
#import "ProfileViewController.h"


@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
      UIWebView *webView = [[UIWebView alloc] init];
    NSString *path = [[NSBundle mainBundle] pathForResource:@"index" ofType:@"html"];
    NSFileHandle *readHandle = [NSFileHandle fileHandleForReadingAtPath:path];
    NSString *htmlString = [[NSString alloc] initWithData:
                            [readHandle readDataToEndOfFile] encoding:NSUTF8StringEncoding];
    
    NSString *basePath = [[NSBundle mainBundle] bundlePath];
    NSURL *baseURL = [NSURL fileURLWithPath:basePath];
    //webView.delegate= self;
    [webView loadHTMLString:htmlString baseURL:baseURL];
    [webView setDataDetectorTypes:UIDataDetectorTypeAddress];
    [webView setDataDetectorTypes:UIDataDetectorTypeCalendarEvent];
    [webView setDataDetectorTypes:UIDataDetectorTypeLink];
    webView.scrollView.scrollEnabled = YES;
    webView.scrollView.bounces = NO;

    ReapWebObject *reapWebObject =[ReapWebObject sharedWebView];
    webView.delegate = reapWebObject;
    reapWebObject.reapWebDelegate= self;
    [reapWebObject setWebView:webView];
    
    // Override point for customization after application launch.
    UIViewController *viewController1 = [[BookmarksViewController alloc] initWithNibName:@"BookmarksViewController" bundle:nil];
    UINavigationController *bookmarkNav = [[UINavigationController alloc] initWithRootViewController:viewController1];
    //TODO need to get the right navbar image
    UIImage *toolBarImg = [UIImage imageNamed:@"navbar"];
    [[UINavigationBar appearance] setBackgroundImage:toolBarImg forBarMetrics:UIBarMetricsDefault];
    
//    UINavigationBar *bar = [bookmarkNav navigationBar];
  //  [bar setTintColor:[UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:1.0]];
    

    UIViewController *viewController2 = [[SocialmarksViewController alloc] initWithNibName:@"SocialmarksViewController" bundle:nil];
    UINavigationController *socialNav = [[UINavigationController alloc] initWithRootViewController:viewController2];
    
    UIViewController *viewController3 = [[CameraViewController alloc] initWithNibName:@"CameraViewController" bundle:nil];
    UINavigationController *cameraNav = [[UINavigationController alloc] initWithRootViewController:viewController3];
    
    UIViewController *viewController4 = [[FavoritesViewController alloc] initWithNibName:@"FavoritesViewController" bundle:nil];
    UINavigationController *favNav = [[UINavigationController alloc] initWithRootViewController:viewController4];
    
        UIViewController *viewController5 = [[ProfileViewController alloc] initWithNibName:@"ProfileViewController" bundle:nil];
    UINavigationController *profileNav = [[UINavigationController alloc] initWithRootViewController:viewController5];
    
    self.tabBarController = [[UITabBarController alloc] init];
    self.tabBarController.viewControllers = @[bookmarkNav,socialNav,cameraNav,favNav,profileNav];
    self.window.rootViewController = self.tabBarController;
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

/*
// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
}
*/

/*
// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didEndCustomizingViewControllers:(NSArray *)viewControllers changed:(BOOL)changed
{
}
*/
-(void)changeTabs:(int)index
{
    [self.tabBarController setSelectedIndex:index];   
}
@end
