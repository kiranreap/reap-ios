//
//  ReapWebObject.m
//  Reap
//
//  Created by Kiran Patchigolla on 11/7/12.
//  Copyright (c) 2012 Kiran Patchigolla. All rights reserved.
//

#import "ReapWebObject.h"

@implementation ReapWebObject
@synthesize webView=_webView;
@synthesize locationController=_locationController;
static ReapWebObject* _sharedWebView = nil;
@synthesize s3;
@synthesize reapWebDelegate;

+(ReapWebObject*)sharedWebView
{
	@synchronized([ReapWebObject class])
	{
		if (!_sharedWebView)
			[[self alloc] init];
        
		return _sharedWebView;
	}
    
	return nil;
}

+(id)alloc
{
	@synchronized([ReapWebObject class])
	{
		NSAssert(_sharedWebView == nil, @"Attempted to allocate a second instance of a singleton.");
		_sharedWebView = [super alloc];
		return _sharedWebView;
	}
    
	return nil;
}

-(id)init {
	self = [super init];
	if (self != nil) {
		// initialize stuff here
        if(![ACCESS_KEY_ID isEqualToString:@"CHANGE ME"]
           && self.s3 == nil)
        {
            // Initial the S3 Client.
            self.s3 = [[AmazonS3Client alloc] initWithAccessKey:ACCESS_KEY_ID withSecretKey:SECRET_KEY] ;
            
            // Create the picture bucket.
            S3CreateBucketRequest *createBucketRequest = [[S3CreateBucketRequest alloc] initWithName:PICTURE_BUCKET] ;
            S3CreateBucketResponse *createBucketResponse = [self.s3 createBucket:createBucketRequest];
            if(createBucketResponse.error != nil)
            {
                NSLog(@"Error: %@", createBucketResponse.error);
            }
        }
        
	}
    
	return self;
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSString *requestString = [[request URL] absoluteString];
    if ([[[request URL] scheme] isEqual:@"mailto"]) {
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    }
    if ([requestString hasPrefix:@"js-frame:"]) {
        
        NSArray *components = [requestString componentsSeparatedByString:@":"];
        NSString *function = [(NSString*)[components objectAtIndex:1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//        NSString *argsAsString = [(NSString*)[components objectAtIndex:2]
  //                                stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
    //    NSError *e = nil;

        if ([function isEqualToString:@"getBestLocation"])
        {
            [self getUserLocation];
        }
        else if ([function isEqualToString:@"selectTab"])
        {
            NSString *selectTab = [(NSString*)[components objectAtIndex:2]
                                      stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            [self.reapWebDelegate changeTabs:[selectTab intValue]];
        }
        else if ([function isEqualToString:@"uploadImage"])
        {
            NSString *imagePath = [(NSString*)[components objectAtIndex:2]
                                   stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            [self uploadImage:imagePath];
        }
        
                   return NO;

    }
    return YES;
 
}
-(void) navigateToHome
{
    
//    Reap.navigate(path, trigger)
    [self.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"Reap.navigate('%@',true)", @"activity"]];

}
- (void)locationUpdate:(CLLocation *)location
{
    CLLocationCoordinate2D coord =[location coordinate];
    
    NSMutableDictionary *locs = [[NSMutableDictionary alloc] init];
    [locs setObject:[NSString stringWithFormat:@"%f",coord.latitude] forKey:@"latitude"];
    [locs setObject:[NSString stringWithFormat:@"%f",coord.longitude] forKey:@"longitude"];
    [locs setObject:[NSString stringWithFormat:@"%f",[location horizontalAccuracy]] forKey:@"accuracy"];
    [locs setObject:[NSString stringWithFormat:@"%f",[location speed]] forKey:@"time"];

    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:locs
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        [self.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@" NativeHost.setBestLocation('%@')",jsonString]];
    }    

}

- (void)locationError:(NSError *)error {

}

//KKPTODO ideally delegates this to a S3 utility
-(void) uploadImage:(NSString *)imagePath
{
    UIImage *image = [UIImage imageNamed:imagePath];//KKPTODO ensure works with path too
    NSString *imageName = [imagePath lastPathComponent];
    NSLog(@" image name %@",imageName);
    // Convert the image to JPEG data.
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
    // Upload image data.  Remember to set the content type.
    S3PutObjectRequest *por = [[S3PutObjectRequest alloc] initWithKey:imageName
                                                             inBucket:PICTURE_BUCKET] ;
    por.contentType = @"image/jpeg";
    por.data = imageData;
    
    // Put the image data into the specified s3 bucket and object.
    [self.s3 putObject:por];

}
-(void) getUserLocation
{
    [self.locationController.locationManager startUpdatingLocation];
//       [self.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"setUserLocation('%@')", @"1111"]];
}
@end
