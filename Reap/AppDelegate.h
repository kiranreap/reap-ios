//
//  AppDelegate.h
//  Reap
//
//  Created by Kiran Patchigolla on 11/1/12.
//  Copyright (c) 2012 Kiran Patchigolla. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReapWebObject.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate, UITabBarControllerDelegate,ReapWebDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UITabBarController *tabBarController;

@end
