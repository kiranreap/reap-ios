//
//  ProfileViewController.m
//  Reap
//
//  Created by Kiran Patchigolla on 11/1/12.
//  Copyright (c) 2012 Kiran Patchigolla. All rights reserved.
//

#import "ProfileViewController.h"
#import "ReapWebObject.h"
@interface ProfileViewController ()

@end

@implementation ProfileViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
                    self.tabBarItem.image = [UIImage imageNamed:@"settings"];        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    UIWebView *webView = [[ReapWebObject sharedWebView] webView];
    webView.frame = self.view.frame;
    [webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"Reap.navigate('%@',true)", @"settings"]];
    
    [self.view addSubview:webView];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
