//
//  MyCLController.m
//  Reap
//
//  Created by Kiran Patchigolla on 11/8/12.
//  Copyright (c) 2012 Kiran Patchigolla. All rights reserved.
//

#import "MyCLController.h"

@implementation MyCLController

@synthesize locationManager;
@synthesize delegate1;

- (id) init {
    self = [super init];
    if (self != nil) {
        self.locationManager = [[CLLocationManager alloc] init];
      self.locationManager.delegate = self; // send loc updates to myself
        self.locationManager.pausesLocationUpdatesAutomatically=YES;//save battery life, if there are no major changes in the location, pause the updates

    }
    return self;
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
   NSLog(@"Location: %@", [newLocation description]);//KKPTODO, to remove this
    [self.delegate1 locationUpdate:newLocation];
    [self.locationManager stopUpdatingLocation];
    //save battery life, if there are no major changes in the location, stop the updates and start again in 2 minutes
    [NSTimer scheduledTimerWithTimeInterval:120.0
                                     target:self
                                   selector:@selector(startAgain:)
                                   userInfo:nil
                                    repeats:NO];
}

-(void)startAgain:(NSTimer *)timer
{
     [self.locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
 [self.delegate1 locationError:error];
}

- (void)dealloc {

}

@end