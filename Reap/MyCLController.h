//
//  MyCLController.h
//  Reap
//
//  Created by Kiran Patchigolla on 11/8/12.
//  Copyright (c) 2012 Kiran Patchigolla. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol MyCLControllerDelegate
@required
- (void)locationUpdate:(CLLocation *)location;
- (void)locationError:(NSError *)error;
@end
@interface MyCLController : NSObject <CLLocationManagerDelegate> {
	CLLocationManager *locationManager;
    id delegate;

}
@property (nonatomic, assign) id  delegate1;
@property (nonatomic, retain) CLLocationManager *locationManager;

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation;

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error;

@end