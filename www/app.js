(function(){
jade = (function(exports){
/*!
 * Jade - runtime
 * Copyright(c) 2010 TJ Holowaychuk <tj@vision-media.ca>
 * MIT Licensed
 */

/**
 * Lame Array.isArray() polyfill for now.
 */

if (!Array.isArray) {
  Array.isArray = function(arr){
    return '[object Array]' == Object.prototype.toString.call(arr);
  };
}

/**
 * Lame Object.keys() polyfill for now.
 */

if (!Object.keys) {
  Object.keys = function(obj){
    var arr = [];
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) {
        arr.push(key);
      }
    }
    return arr;
  }
}

/**
 * Merge two attribute objects giving precedence
 * to values in object `b`. Classes are special-cased
 * allowing for arrays and merging/joining appropriately
 * resulting in a string.
 *
 * @param {Object} a
 * @param {Object} b
 * @return {Object} a
 * @api private
 */

exports.merge = function merge(a, b) {
  var ac = a['class'];
  var bc = b['class'];

  if (ac || bc) {
    ac = ac || [];
    bc = bc || [];
    if (!Array.isArray(ac)) ac = [ac];
    if (!Array.isArray(bc)) bc = [bc];
    ac = ac.filter(nulls);
    bc = bc.filter(nulls);
    a['class'] = ac.concat(bc).join(' ');
  }

  for (var key in b) {
    if (key != 'class') {
      a[key] = b[key];
    }
  }

  return a;
};

/**
 * Filter null `val`s.
 *
 * @param {Mixed} val
 * @return {Mixed}
 * @api private
 */

function nulls(val) {
  return val != null;
}

/**
 * Render the given attributes object.
 *
 * @param {Object} obj
 * @param {Object} escaped
 * @return {String}
 * @api private
 */

exports.attrs = function attrs(obj, escaped){
  var buf = []
    , terse = obj.terse;

  delete obj.terse;
  var keys = Object.keys(obj)
    , len = keys.length;

  if (len) {
    buf.push('');
    for (var i = 0; i < len; ++i) {
      var key = keys[i]
        , val = obj[key];

      if ('boolean' == typeof val || null == val) {
        if (val) {
          terse
            ? buf.push(key)
            : buf.push(key + '="' + key + '"');
        }
      } else if (0 == key.indexOf('data') && 'string' != typeof val) {
        buf.push(key + "='" + JSON.stringify(val) + "'");
      } else if ('class' == key && Array.isArray(val)) {
        buf.push(key + '="' + exports.escape(val.join(' ')) + '"');
      } else if (escaped && escaped[key]) {
        buf.push(key + '="' + exports.escape(val) + '"');
      } else {
        buf.push(key + '="' + val + '"');
      }
    }
  }

  return buf.join(' ');
};

/**
 * Escape the given string of `html`.
 *
 * @param {String} html
 * @return {String}
 * @api private
 */

exports.escape = function escape(html){
  return String(html)
    .replace(/&(?!(\w+|\#\d+);)/g, '&amp;')
    .replace(/</g, '&lt;')
    .replace(/>/g, '&gt;')
    .replace(/"/g, '&quot;');
};

/**
 * Re-throw the given `err` in context to the
 * the jade in `filename` at the given `lineno`.
 *
 * @param {Error} err
 * @param {String} filename
 * @param {String} lineno
 * @api private
 */

exports.rethrow = function rethrow(err, filename, lineno){
  if (!filename) throw err;

  var context = 3
    , str = require('fs').readFileSync(filename, 'utf8')
    , lines = str.split('\n')
    , start = Math.max(lineno - context, 0)
    , end = Math.min(lines.length, lineno + context);

  // Error context
  var context = lines.slice(start, end).map(function(line, i){
    var curr = i + start + 1;
    return (curr == lineno ? '  > ' : '    ')
      + curr
      + '| '
      + line;
  }).join('\n');

  // Alter exception message
  err.path = filename;
  err.message = (filename || 'Jade') + ':' + lineno
    + '\n' + context + '\n\n' + err.message;
  throw err;
};

  return exports;

})({});

var require = function (file, cwd) {
    var resolved = require.resolve(file, cwd || '/');
    var mod = require.modules[resolved];
    if (!mod) throw new Error(
        'Failed to resolve module ' + file + ', tried ' + resolved
    );
    var cached = require.cache[resolved];
    var res = cached? cached.exports : mod();
    return res;
};

require.paths = [];
require.modules = {};
require.cache = {};
require.extensions = [".js",".coffee",".json",".jade"];

require._core = {
    'assert': true,
    'events': true,
    'fs': true,
    'path': true,
    'vm': true
};

require.resolve = (function () {
    return function (x, cwd) {
        if (!cwd) cwd = '/';
        
        if (require._core[x]) return x;
        var path = require.modules.path();
        cwd = path.resolve('/', cwd);
        var y = cwd || '/';
        
        if (x.match(/^(?:\.\.?\/|\/)/)) {
            var m = loadAsFileSync(path.resolve(y, x))
                || loadAsDirectorySync(path.resolve(y, x));
            if (m) return m;
        }
        
        var n = loadNodeModulesSync(x, y);
        if (n) return n;
        
        throw new Error("Cannot find module '" + x + "'");
        
        function loadAsFileSync (x) {
            x = path.normalize(x);
            if (require.modules[x]) {
                return x;
            }
            
            for (var i = 0; i < require.extensions.length; i++) {
                var ext = require.extensions[i];
                if (require.modules[x + ext]) return x + ext;
            }
        }
        
        function loadAsDirectorySync (x) {
            x = x.replace(/\/+$/, '');
            var pkgfile = path.normalize(x + '/package.json');
            if (require.modules[pkgfile]) {
                var pkg = require.modules[pkgfile]();
                var b = pkg.browserify;
                if (typeof b === 'object' && b.main) {
                    var m = loadAsFileSync(path.resolve(x, b.main));
                    if (m) return m;
                }
                else if (typeof b === 'string') {
                    var m = loadAsFileSync(path.resolve(x, b));
                    if (m) return m;
                }
                else if (pkg.main) {
                    var m = loadAsFileSync(path.resolve(x, pkg.main));
                    if (m) return m;
                }
            }
            
            return loadAsFileSync(x + '/index');
        }
        
        function loadNodeModulesSync (x, start) {
            var dirs = nodeModulesPathsSync(start);
            for (var i = 0; i < dirs.length; i++) {
                var dir = dirs[i];
                var m = loadAsFileSync(dir + '/' + x);
                if (m) return m;
                var n = loadAsDirectorySync(dir + '/' + x);
                if (n) return n;
            }
            
            var m = loadAsFileSync(x);
            if (m) return m;
        }
        
        function nodeModulesPathsSync (start) {
            var parts;
            if (start === '/') parts = [ '' ];
            else parts = path.normalize(start).split('/');
            
            var dirs = [];
            for (var i = parts.length - 1; i >= 0; i--) {
                if (parts[i] === 'node_modules') continue;
                var dir = parts.slice(0, i + 1).join('/') + '/node_modules';
                dirs.push(dir);
            }
            
            return dirs;
        }
    };
})();

require.alias = function (from, to) {
    var path = require.modules.path();
    var res = null;
    try {
        res = require.resolve(from + '/package.json', '/');
    }
    catch (err) {
        res = require.resolve(from, '/');
    }
    var basedir = path.dirname(res);
    
    var keys = (Object.keys || function (obj) {
        var res = [];
        for (var key in obj) res.push(key);
        return res;
    })(require.modules);
    
    for (var i = 0; i < keys.length; i++) {
        var key = keys[i];
        if (key.slice(0, basedir.length + 1) === basedir + '/') {
            var f = key.slice(basedir.length);
            require.modules[to + f] = require.modules[basedir + f];
        }
        else if (key === basedir) {
            require.modules[to] = require.modules[basedir];
        }
    }
};

(function () {
    var process = {};
    var global = typeof window !== 'undefined' ? window : {};
    var definedProcess = false;
    
    require.define = function (filename, fn) {
        if (!definedProcess && require.modules.__browserify_process) {
            process = require.modules.__browserify_process();
            definedProcess = true;
        }
        
        var dirname = require._core[filename]
            ? ''
            : require.modules.path().dirname(filename)
        ;
        
        var require_ = function (file) {
            var requiredModule = require(file, dirname);
            var cached = require.cache[require.resolve(file, dirname)];

            if (cached && cached.parent === null) {
                cached.parent = module_;
            }

            return requiredModule;
        };
        require_.resolve = function (name) {
            return require.resolve(name, dirname);
        };
        require_.modules = require.modules;
        require_.define = require.define;
        require_.cache = require.cache;
        var module_ = {
            id : filename,
            filename: filename,
            exports : {},
            loaded : false,
            parent: null
        };
        
        require.modules[filename] = function () {
            require.cache[filename] = module_;
            fn.call(
                module_.exports,
                require_,
                module_,
                module_.exports,
                dirname,
                filename,
                process,
                global
            );
            module_.loaded = true;
            return module_.exports;
        };
    };
})();


require.define("path",Function(['require','module','exports','__dirname','__filename','process','global'],"function filter (xs, fn) {\n    var res = [];\n    for (var i = 0; i < xs.length; i++) {\n        if (fn(xs[i], i, xs)) res.push(xs[i]);\n    }\n    return res;\n}\n\n// resolves . and .. elements in a path array with directory names there\n// must be no slashes, empty elements, or device names (c:\\) in the array\n// (so also no leading and trailing slashes - it does not distinguish\n// relative and absolute paths)\nfunction normalizeArray(parts, allowAboveRoot) {\n  // if the path tries to go above the root, `up` ends up > 0\n  var up = 0;\n  for (var i = parts.length; i >= 0; i--) {\n    var last = parts[i];\n    if (last == '.') {\n      parts.splice(i, 1);\n    } else if (last === '..') {\n      parts.splice(i, 1);\n      up++;\n    } else if (up) {\n      parts.splice(i, 1);\n      up--;\n    }\n  }\n\n  // if the path is allowed to go above the root, restore leading ..s\n  if (allowAboveRoot) {\n    for (; up--; up) {\n      parts.unshift('..');\n    }\n  }\n\n  return parts;\n}\n\n// Regex to split a filename into [*, dir, basename, ext]\n// posix version\nvar splitPathRe = /^(.+\\/(?!$)|\\/)?((?:.+?)?(\\.[^.]*)?)$/;\n\n// path.resolve([from ...], to)\n// posix version\nexports.resolve = function() {\nvar resolvedPath = '',\n    resolvedAbsolute = false;\n\nfor (var i = arguments.length; i >= -1 && !resolvedAbsolute; i--) {\n  var path = (i >= 0)\n      ? arguments[i]\n      : process.cwd();\n\n  // Skip empty and invalid entries\n  if (typeof path !== 'string' || !path) {\n    continue;\n  }\n\n  resolvedPath = path + '/' + resolvedPath;\n  resolvedAbsolute = path.charAt(0) === '/';\n}\n\n// At this point the path should be resolved to a full absolute path, but\n// handle relative paths to be safe (might happen when process.cwd() fails)\n\n// Normalize the path\nresolvedPath = normalizeArray(filter(resolvedPath.split('/'), function(p) {\n    return !!p;\n  }), !resolvedAbsolute).join('/');\n\n  return ((resolvedAbsolute ? '/' : '') + resolvedPath) || '.';\n};\n\n// path.normalize(path)\n// posix version\nexports.normalize = function(path) {\nvar isAbsolute = path.charAt(0) === '/',\n    trailingSlash = path.slice(-1) === '/';\n\n// Normalize the path\npath = normalizeArray(filter(path.split('/'), function(p) {\n    return !!p;\n  }), !isAbsolute).join('/');\n\n  if (!path && !isAbsolute) {\n    path = '.';\n  }\n  if (path && trailingSlash) {\n    path += '/';\n  }\n  \n  return (isAbsolute ? '/' : '') + path;\n};\n\n\n// posix version\nexports.join = function() {\n  var paths = Array.prototype.slice.call(arguments, 0);\n  return exports.normalize(filter(paths, function(p, index) {\n    return p && typeof p === 'string';\n  }).join('/'));\n};\n\n\nexports.dirname = function(path) {\n  var dir = splitPathRe.exec(path)[1] || '';\n  var isWindows = false;\n  if (!dir) {\n    // No dirname\n    return '.';\n  } else if (dir.length === 1 ||\n      (isWindows && dir.length <= 3 && dir.charAt(1) === ':')) {\n    // It is just a slash or a drive letter with a slash\n    return dir;\n  } else {\n    // It is a full dirname, strip trailing slash\n    return dir.substring(0, dir.length - 1);\n  }\n};\n\n\nexports.basename = function(path, ext) {\n  var f = splitPathRe.exec(path)[2] || '';\n  // TODO: make this comparison case-insensitive on windows?\n  if (ext && f.substr(-1 * ext.length) === ext) {\n    f = f.substr(0, f.length - ext.length);\n  }\n  return f;\n};\n\n\nexports.extname = function(path) {\n  return splitPathRe.exec(path)[3] || '';\n};\n\n//@ sourceURL=path"
));

require.define("__browserify_process",Function(['require','module','exports','__dirname','__filename','process','global'],"var process = module.exports = {};\n\nprocess.nextTick = (function () {\n    var canSetImmediate = typeof window !== 'undefined'\n        && window.setImmediate;\n    var canPost = typeof window !== 'undefined'\n        && window.postMessage && window.addEventListener\n    ;\n\n    if (canSetImmediate) {\n        return window.setImmediate;\n    }\n\n    if (canPost) {\n        var queue = [];\n        window.addEventListener('message', function (ev) {\n            if (ev.source === window && ev.data === 'browserify-tick') {\n                ev.stopPropagation();\n                if (queue.length > 0) {\n                    var fn = queue.shift();\n                    fn();\n                }\n            }\n        }, true);\n\n        return function nextTick(fn) {\n            queue.push(fn);\n            window.postMessage('browserify-tick', '*');\n        };\n    }\n\n    return function nextTick(fn) {\n        setTimeout(fn, 0);\n    };\n})();\n\nprocess.title = 'browser';\nprocess.browser = true;\nprocess.env = {};\nprocess.argv = [];\n\nprocess.binding = function (name) {\n    if (name === 'evals') return (require)('vm')\n    else throw new Error('No such module. (Possibly not yet loaded)')\n};\n\n(function () {\n    var cwd = '/';\n    var path;\n    process.cwd = function () { return cwd };\n    process.chdir = function (dir) {\n        if (!path) path = require('path');\n        cwd = path.resolve(dir, cwd);\n    };\n})();\n\n//@ sourceURL=__browserify_process"
));

require.define("/util.coffee",Function(['require','module','exports','__dirname','__filename','process','global'],"(function() {\n\n  Array.prototype.random = function() {\n    return this[Math.floor(Math.random() * this.length)];\n  };\n\n  window.delay = function(delay, func) {\n    return setTimeout(func, delay);\n  };\n\n  module.exports = {\n    randomInt: function(a, b) {\n      var max, min;\n      b || (b = 0);\n      max = a > b ? a : b;\n      min = a > b ? b : a;\n      return Math.floor(Math.random() * (max - min) + min);\n    }\n  };\n\n}).call(this);\n\n//@ sourceURL=/util.coffee"
));

require.define("/models/uzer.coffee",Function(['require','module','exports','__dirname','__filename','process','global'],"(function() {\n  var md5;\n\n  md5 = require('../libs/md5.js');\n\n  module.exports = {\n    initialize: function() {\n      var uzer;\n      uzer = localStorage['uzer'];\n      if (uzer) {\n        try {\n          uzer = JSON.parse(uzer);\n        } catch (e) {\n          localStorage['uzer'] = '';\n          uzer = this.doinit();\n        }\n        this.onlogin(uzer);\n      } else {\n        uzer = this.doinit();\n      }\n      return window.Reap.uzer = uzer;\n    },\n    getSnapshops: function() {\n      var snapshops;\n      snapshops = require('./snapshop');\n      snapshops.initialize();\n      return snapshops.snapshops;\n    },\n    doinit: function() {\n      var username, uzer;\n      username = \"xnov-common\";\n      localStorage['uzer'] = JSON.stringify({\n        username: username\n      });\n      uzer = JSON.parse(localStorage['uzer']);\n      return uzer;\n    },\n    isanon: function() {\n      if (window.Reap.uzer.username.indexOf('xnov-') !== -1) {\n        return true;\n      }\n      return false;\n    },\n    validateEmail: function (email) { \n        var re = /^(([^<>()[\\]\\\\.,;:\\s@\\\"]+(\\.[^<>()[\\]\\\\.,;:\\s@\\\"]+)*)|(\\\".+\\\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$/;\n        return re.test(email);\n        } ,\n    validateUsername: function (username) { \n        if (username.length <5) { return false; }\n        var re = /^([a-zA-Z0-9.]+@){0,1}([a-zA-Z0-9.])+$/;\n        return re.test(username);\n        },\n    validatePassword: function(password) {\n      if (password.length < 6) {\n        return false;\n      }\n      return true;\n    },\n    passwordHash: function(password) {\n      return md5(password);\n    },\n    onlogin: function(uzer) {\n      localStorage['uzer'] = JSON.stringify(uzer);\n      if (uzer.username) {\n        window.NativeHost.setPushChannel(uzer.username);\n      }\n      if (!localStorage['snapshops']) {\n        uzer.snapshops = this.getSnapshops();\n      }\n      return window.Reap.uzer = uzer;\n    },\n    logout: function(cb) {\n      localStorage['uzer'] = '';\n      localStorage['usnapshops'] = '';\n      this.initialize();\n      return cb();\n    },\n    login: function(username, password, cb) {\n      var filter, self, xhr,\n        _this = this;\n      self = this;\n      filter = {\n        username: username,\n        password: self.passwordHash(password)\n      };\n      return xhr = $.ajax({\n        url: 'https://api.parse.com/1/classes/Uzer',\n        dataType: 'json',\n        headers: {\n          'X-Parse-Application-Id': window.global.PARSE_ID,\n          'X-Parse-REST-API-Key': window.global.PARSE_KEY\n        },\n        contentType: 'application/json; charset=utf-8',\n        data: {\n          where: filter\n        },\n        success: function(data) {\n          var uzer;\n          if (data['results']) {\n            uzer = data['results'][0];\n            if (uzer) {\n              self.onlogin(uzer);\n              return cb();\n            } else {\n              return cb(\"Invalid user / password\");\n            }\n          }\n        },\n        error: function(jqXHR, textStatus, errorThrown) {\n          NativeHost.toast(\"POST failed: \" + errorThrown);\n          console.error(errorThrown);\n          return console.error(JSON.stringify(jqXHR));\n        }\n      });\n    },\n    signup: function(username, password, email, cb) {\n      var indata, self;\n      self = this;\n      indata = {\n        username: username,\n        password: self.passwordHash(password),\n        email: email\n      };\n      return Parse.Cloud.run('signup', indata, {\n        success: function(data) {\n          var uzer;\n          if (data.error) {\n            cb(data.error);\n            return;\n          }\n          if (data.get(\"username\")) {\n            uzer = data.toJSON();\n            uzer.objectId = data.id;\n            uzer.createdAt = data.createdAt;\n            uzer.updatedAt = data.updatedAt;\n            self.onlogin(uzer);\n            return cb();\n          }\n        }\n      });\n    }\n  };\n\n}).call(this);\n\n//@ sourceURL=/models/uzer.coffee"
));

require.define("/libs/md5.js",Function(['require','module','exports','__dirname','__filename','process','global'],"function md5cycle(x, k) {\nvar a = x[0], b = x[1], c = x[2], d = x[3];\n\na = ff(a, b, c, d, k[0], 7, -680876936);\nd = ff(d, a, b, c, k[1], 12, -389564586);\nc = ff(c, d, a, b, k[2], 17,  606105819);\nb = ff(b, c, d, a, k[3], 22, -1044525330);\na = ff(a, b, c, d, k[4], 7, -176418897);\nd = ff(d, a, b, c, k[5], 12,  1200080426);\nc = ff(c, d, a, b, k[6], 17, -1473231341);\nb = ff(b, c, d, a, k[7], 22, -45705983);\na = ff(a, b, c, d, k[8], 7,  1770035416);\nd = ff(d, a, b, c, k[9], 12, -1958414417);\nc = ff(c, d, a, b, k[10], 17, -42063);\nb = ff(b, c, d, a, k[11], 22, -1990404162);\na = ff(a, b, c, d, k[12], 7,  1804603682);\nd = ff(d, a, b, c, k[13], 12, -40341101);\nc = ff(c, d, a, b, k[14], 17, -1502002290);\nb = ff(b, c, d, a, k[15], 22,  1236535329);\n\na = gg(a, b, c, d, k[1], 5, -165796510);\nd = gg(d, a, b, c, k[6], 9, -1069501632);\nc = gg(c, d, a, b, k[11], 14,  643717713);\nb = gg(b, c, d, a, k[0], 20, -373897302);\na = gg(a, b, c, d, k[5], 5, -701558691);\nd = gg(d, a, b, c, k[10], 9,  38016083);\nc = gg(c, d, a, b, k[15], 14, -660478335);\nb = gg(b, c, d, a, k[4], 20, -405537848);\na = gg(a, b, c, d, k[9], 5,  568446438);\nd = gg(d, a, b, c, k[14], 9, -1019803690);\nc = gg(c, d, a, b, k[3], 14, -187363961);\nb = gg(b, c, d, a, k[8], 20,  1163531501);\na = gg(a, b, c, d, k[13], 5, -1444681467);\nd = gg(d, a, b, c, k[2], 9, -51403784);\nc = gg(c, d, a, b, k[7], 14,  1735328473);\nb = gg(b, c, d, a, k[12], 20, -1926607734);\n\na = hh(a, b, c, d, k[5], 4, -378558);\nd = hh(d, a, b, c, k[8], 11, -2022574463);\nc = hh(c, d, a, b, k[11], 16,  1839030562);\nb = hh(b, c, d, a, k[14], 23, -35309556);\na = hh(a, b, c, d, k[1], 4, -1530992060);\nd = hh(d, a, b, c, k[4], 11,  1272893353);\nc = hh(c, d, a, b, k[7], 16, -155497632);\nb = hh(b, c, d, a, k[10], 23, -1094730640);\na = hh(a, b, c, d, k[13], 4,  681279174);\nd = hh(d, a, b, c, k[0], 11, -358537222);\nc = hh(c, d, a, b, k[3], 16, -722521979);\nb = hh(b, c, d, a, k[6], 23,  76029189);\na = hh(a, b, c, d, k[9], 4, -640364487);\nd = hh(d, a, b, c, k[12], 11, -421815835);\nc = hh(c, d, a, b, k[15], 16,  530742520);\nb = hh(b, c, d, a, k[2], 23, -995338651);\n\na = ii(a, b, c, d, k[0], 6, -198630844);\nd = ii(d, a, b, c, k[7], 10,  1126891415);\nc = ii(c, d, a, b, k[14], 15, -1416354905);\nb = ii(b, c, d, a, k[5], 21, -57434055);\na = ii(a, b, c, d, k[12], 6,  1700485571);\nd = ii(d, a, b, c, k[3], 10, -1894986606);\nc = ii(c, d, a, b, k[10], 15, -1051523);\nb = ii(b, c, d, a, k[1], 21, -2054922799);\na = ii(a, b, c, d, k[8], 6,  1873313359);\nd = ii(d, a, b, c, k[15], 10, -30611744);\nc = ii(c, d, a, b, k[6], 15, -1560198380);\nb = ii(b, c, d, a, k[13], 21,  1309151649);\na = ii(a, b, c, d, k[4], 6, -145523070);\nd = ii(d, a, b, c, k[11], 10, -1120210379);\nc = ii(c, d, a, b, k[2], 15,  718787259);\nb = ii(b, c, d, a, k[9], 21, -343485551);\n\nx[0] = add32(a, x[0]);\nx[1] = add32(b, x[1]);\nx[2] = add32(c, x[2]);\nx[3] = add32(d, x[3]);\n\n}\n\nfunction cmn(q, a, b, x, s, t) {\na = add32(add32(a, q), add32(x, t));\nreturn add32((a << s) | (a >>> (32 - s)), b);\n}\n\nfunction ff(a, b, c, d, x, s, t) {\nreturn cmn((b & c) | ((~b) & d), a, b, x, s, t);\n}\n\nfunction gg(a, b, c, d, x, s, t) {\nreturn cmn((b & d) | (c & (~d)), a, b, x, s, t);\n}\n\nfunction hh(a, b, c, d, x, s, t) {\nreturn cmn(b ^ c ^ d, a, b, x, s, t);\n}\n\nfunction ii(a, b, c, d, x, s, t) {\nreturn cmn(c ^ (b | (~d)), a, b, x, s, t);\n}\n\nfunction md51(s) {\ntxt = '';\nvar n = s.length,\nstate = [1732584193, -271733879, -1732584194, 271733878], i;\nfor (i=64; i<=s.length; i+=64) {\nmd5cycle(state, md5blk(s.substring(i-64, i)));\n}\ns = s.substring(i-64);\nvar tail = [0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0];\nfor (i=0; i<s.length; i++)\ntail[i>>2] |= s.charCodeAt(i) << ((i%4) << 3);\ntail[i>>2] |= 0x80 << ((i%4) << 3);\nif (i > 55) {\nmd5cycle(state, tail);\nfor (i=0; i<16; i++) tail[i] = 0;\n}\ntail[14] = n*8;\nmd5cycle(state, tail);\nreturn state;\n}\n\n/* there needs to be support for Unicode here,\n * unless we pretend that we can redefine the MD-5\n * algorithm for multi-byte characters (perhaps\n * by adding every four 16-bit characters and\n * shortening the sum to 32 bits). Otherwise\n * I suggest performing MD-5 as if every character\n * was two bytes--e.g., 0040 0025 = @%--but then\n * how will an ordinary MD-5 sum be matched?\n * There is no way to standardize text to something\n * like UTF-8 before transformation; speed cost is\n * utterly prohibitive. The JavaScript standard\n * itself needs to look at this: it should start\n * providing access to strings as preformed UTF-8\n * 8-bit unsigned value arrays.\n */\nfunction md5blk(s) { /* I figured global was faster.   */\nvar md5blks = [], i; /* Andy King said do it this way. */\nfor (i=0; i<64; i+=4) {\nmd5blks[i>>2] = s.charCodeAt(i)\n+ (s.charCodeAt(i+1) << 8)\n+ (s.charCodeAt(i+2) << 16)\n+ (s.charCodeAt(i+3) << 24);\n}\nreturn md5blks;\n}\n\nvar hex_chr = '0123456789abcdef'.split('');\n\nfunction rhex(n)\n{\nvar s='', j=0;\nfor(; j<4; j++)\ns += hex_chr[(n >> (j * 8 + 4)) & 0x0F]\n+ hex_chr[(n >> (j * 8)) & 0x0F];\nreturn s;\n}\n\nfunction hex(x) {\nfor (var i=0; i<x.length; i++)\nx[i] = rhex(x[i]);\nreturn x.join('');\n}\n\nmodule.exports = function (s) {\nreturn hex(md51(s));\n}\n\n/* this function is much faster,\nso if possible we use it. Some IEs\nare the only ones I know of that\nneed the idiotic second function,\ngenerated by an if clause.  */\n\nfunction add32(a, b) {\nreturn (a + b) & 0xFFFFFFFF;\n}\n\n/*\nif (md5('hello') != '5d41402abc4b2a76b9719d911017c592') {\nfunction add32(x, y) {\nvar lsw = (x & 0xFFFF) + (y & 0xFFFF),\nmsw = (x >> 16) + (y >> 16) + (lsw >> 16);\nreturn (msw << 16) | (lsw & 0xFFFF);\n}\n}\n*/\n\n//@ sourceURL=/libs/md5.js"
));

require.define("/models/snapshop.coffee",Function(['require','module','exports','__dirname','__filename','process','global'],"(function() {\n  var snapshopC, snapshopM;\n\n  snapshopM = Backbone.Model.extend({\n    defaults: {\n      \"private\": 'y',\n      status: 'inuse',\n      lastUsed: new Date().getTime()\n    }\n  });\n\n  snapshopC = Backbone.Collection.extend({\n    model: snapshopM,\n    comparator: function(m) {\n      return -m.get('lastUsed');\n    },\n    touch: function(name) {\n      this.each(function(m) {\n        if (m.get(\"name\") === name) {\n          return m.set('lastUsed', new Date().getTime());\n        }\n      });\n      return this.save();\n    },\n    donew: function(name) {\n      this.add({\n        name: name\n      });\n      return this.save();\n    },\n    save: function() {\n      return localStorage['snapshops'] = JSON.stringify(this);\n    }\n  });\n\n  module.exports = {\n    initialize: function(username) {\n      var snapshopsStr;\n      snapshopsStr = localStorage['snapshops'];\n      if (snapshopsStr) {\n        return this.snapshops = new snapshopC(JSON.parse(snapshopsStr));\n      } else {\n        this.snapshops = new snapshopC([\n          {\n            name: 'My Shopping Grabbag'\n          }, {\n            name: 'For the home'\n          }, {\n            name: 'Clothes for the family'\n          }, {\n            name: 'Gifts'\n          }\n        ]);\n        return this.snapshops.save();\n      }\n    }\n  };\n\n}).call(this);\n\n//@ sourceURL=/models/snapshop.coffee"
));

require.define("/snappage/explore.coffee",Function(['require','module','exports','__dirname','__filename','process','global'],"(function() {\n  var onePage, oneRow;\n\n  oneRow = require('./onerow.coffee');\n\n  onePage = require('./onepage.coffee');\n\n  module.exports = Backbone.View.extend({\n    initialize: function(url) {\n      this.url = url;\n      this.template = require('./explore.jade');\n      this.render();\n      return this;\n    },\n    render: function() {\n      var _this = this;\n      this.$el.html(this.template({\n        url: this.url\n      }));\n      this.length = 0;\n      this.$el.off('infiniteScroll');\n      this.$el.on('infiniteScroll', function() {\n        return _this.load();\n      });\n      return this;\n    },\n    xload: function() {\n      if (this.loading) {\n        return;\n      }\n      this.loading = true;\n      console.debug(\"loading \" + this.url);\n      Parse.Cloud.run('geomarks', {}, {\n        success: function(data) {\n          var ar, collection;\n          collection = new Backbone.Collection(data);\n          if (!collection.length) {\n            return;\n          }\n          ar = new onePage({\n            collection: collection,\n            parent: $(\".homepanel\", this.$el)\n          });\n          /*\n                          ar = new oneRow({collection: collection, parent: $(\".homepanel\", @$el)})\n                          ar = new oneRow({collection: collection, parent: $(\".homepanel\", @$el)})\n                          ar = new oneRow({collection: collection, parent: $(\".homepanel\", @$el)})\n                          ar = new oneRow({collection: collection, parent: $(\".homepanel\", @$el)})\n          */\n\n          this.length += collection.length;\n          return this.loading = false;\n        }\n      });\n      return this;\n    },\n    load: function() {\n      var xhr,\n        _this = this;\n      if (this.loading) {\n        return;\n      }\n      this.loading = true;\n      console.log('h ' + window.location.href.toString());\n      if (window.location.href.toString().indexOf('explore') === -1) {\n        return;\n      }\n      console.debug(\"loading \" + this.length);\n      xhr = $.ajax({\n        url: 'https://api.parse.com/1/classes/Mark',\n        dataType: 'json',\n        headers: {\n          'X-Parse-Application-Id': window.global.PARSE_ID,\n          'X-Parse-REST-API-Key': window.global.PARSE_KEY\n        },\n        data: {\n          order: '-createdAt',\n          limit: 12,\n          skip: this.length\n        },\n        success: function(data) {\n          var ar, collection;\n          console.log(\"AJAX success\");\n          collection = new Backbone.Collection(data.results);\n          if (!collection.length) {\n            return;\n          } else {\n            console.log('loaded ' + collection.length);\n          }\n          ar = new onePage({\n            collection: collection,\n            parent: $(\".homepanel\", _this.$el)\n          });\n          _this.length += collection.length;\n          _this.loading = false;\n          return console.debug(\"length is \" + _this.length);\n        },\n        error: function(jqXHR, textStatus, errorThrown) {\n          return console.error(errorThrown);\n        }\n      });\n      xhr.complete(function() {\n        console.log(\"AJAX complete\");\n        return _this.loading = false;\n      });\n      return this;\n    }\n  });\n\n}).call(this);\n\n//@ sourceURL=/snappage/explore.coffee"
));

require.define("/snappage/onerow.coffee",Function(['require','module','exports','__dirname','__filename','process','global'],"(function() {\n  var onePic, onerow_template;\n\n  onerow_template = require('./onerow.jade');\n\n  onePic = require('./onepic');\n\n  module.exports = Backbone.View.extend({\n    initialize: function() {\n      if (!this.collection) {\n        console.log('no collection');\n        this.collection = new Backbone.Collection();\n      }\n      if (!this.collection.length) {\n        return;\n      }\n      return this.render();\n    },\n    render: function() {\n      var crow, self,\n        _this = this;\n      self = this;\n      crow = this.collection.at(0);\n      $(this.el).html(onerow_template({\n        crow: crow.toJSON(),\n        store: 'blah'\n      }));\n      $(this.options.parent).append(this.el);\n      this.collection.each(function(mark) {\n        var op;\n        return op = new onePic({\n          model: mark,\n          parent: $(\".photogrid\", self.el),\n          container: 'row'\n        });\n      });\n      return this;\n    }\n  });\n\n}).call(this);\n\n//@ sourceURL=/snappage/onerow.coffee"
));

require.define("/snappage/onerow.jade",Function(['require','module','exports','__dirname','__filename','process','global'],"module.exports = function anonymous(locals, attrs, escape, rethrow, merge) {\nattrs = attrs || jade.attrs; escape = escape || jade.escape; rethrow = rethrow || jade.rethrow; merge = merge || jade.merge;\nvar buf = [];\nwith (locals || {}) {\nvar interp;\nbuf.push('<div><ul class=\"tabrow\"><li style=\"margin-left:8px\"><a href=\"#search\">' + escape((interp = store) == null ? '' : interp) + '</a></li></ul><div style=\"white-space:nowrap;overflow:scroll\" class=\"photogrid\"></div></div>');\n}\nreturn buf.join(\"\");\n};\n//@ sourceURL=/snappage/onerow.jade"
));

require.define("/snappage/onepic.coffee",Function(['require','module','exports','__dirname','__filename','process','global'],"(function() {\n  var DetailView, onepic_template;\n\n  onepic_template = require('./onepic.jade');\n\n  DetailView = require('./detail');\n\n  module.exports = Backbone.View.extend({\n    initialize: function() {\n      if (!this.model) {\n        console.log('no model');\n        this.model = new Backbone.Model();\n      }\n      if (!this.options.container) {\n        this.options.container = 'page';\n      }\n      return this.render();\n    },\n    events: {\n      \"click img.mark\": \"showDetails\"\n    },\n    render: function() {\n      var self, serverdata, w;\n      if (this.model.get(\"_serverData\")) {\n        serverdata = this.model.get(\"_serverData\");\n        this.model.set(\"objectId\", this.model.id);\n        this.model.set(\"createdAt\", this.model.createdAt);\n        this.model.set(\"updatedAt\", this.model.updatedAt);\n      } else {\n        serverdata = this.model.toJSON();\n      }\n      self = this;\n      w = (($(window).width() / 3) - 6).toString().split('.')[0];\n      $(this.el).html(onepic_template({\n        mark: serverdata,\n        w: w\n      }));\n      if (this.options.container === 'row') {\n        $(this.el).css({\n          'display': 'inline-block'\n        });\n      } else {\n        $(this.el).css({\n          'float': 'left'\n        });\n      }\n      $(this.options.parent).append(this.el);\n      return this;\n    },\n    showDetails: function(e) {\n      var dview, self;\n      self = this;\n      window.Reap.navigate('details/' + this.model.get('objectId'), false);\n      return dview = new DetailView({\n        model: self.model\n      });\n    }\n  });\n\n}).call(this);\n\n//@ sourceURL=/snappage/onepic.coffee"
));

require.define("/snappage/onepic.jade",Function(['require','module','exports','__dirname','__filename','process','global'],"module.exports = function anonymous(locals, attrs, escape, rethrow, merge) {\nattrs = attrs || jade.attrs; escape = escape || jade.escape; rethrow = rethrow || jade.rethrow; merge = merge || jade.merge;\nvar buf = [];\nwith (locals || {}) {\nvar interp;\nbuf.push('<div');\nbuf.push(attrs({ 'style':('float:left;width:' + (w) + 'px;height:' + (w) + 'px;padding:2px;margin-bottom:5px;overflow:hidden') }, {\"style\":true}));\nbuf.push('><img');\nbuf.push(attrs({ 'src':(\"\" + (mark.imgSrc) + \"\"), 'style':('float:left;width:' + (w) + 'px'), \"class\": ('mark') }, {\"src\":true,\"style\":true}));\nbuf.push('/></div>');\n}\nreturn buf.join(\"\");\n};\n//@ sourceURL=/snappage/onepic.jade"
));

require.define("/snappage/detail.coffee",Function(['require','module','exports','__dirname','__filename','process','global'],"(function() {\n  var oneComment, uzerM;\n\n  oneComment = require('./onecomment');\n\n  uzerM = require('../models/uzer');\n\n  module.exports = Backbone.View.extend({\n    initialize: function() {\n      var markId, self, xhr,\n        _this = this;\n      this.template = require('./detail.jade');\n      this.mapisup = false;\n      this.addcommentisup = false;\n      if (this.model) {\n        this.render();\n      } else {\n        self = this;\n        NativeHost.toast(\"loading \" + location.hash);\n        markId = location.hash.toString().split(\"/\").pop();\n        xhr = $.ajax({\n          url: 'https://api.parse.com/1/classes/Mark/' + markId,\n          dataType: 'json',\n          type: 'GET',\n          headers: {\n            'X-Parse-Application-Id': window.global.PARSE_ID,\n            'X-Parse-REST-API-Key': window.global.PARSE_KEY\n          },\n          success: function(data) {\n            console.log(\"AJAX success\");\n            self.model = new Backbone.Model(data);\n            return self.render();\n          },\n          error: function(jqXHR, textStatus, errorThrown) {\n            return console.error(errorThrown);\n          }\n        });\n      }\n      return this;\n    },\n    events: {\n      \"tap .domap\": \"domap\",\n      \"tap .addcomment\": \"addcomment\",\n      \"tap .dotest\": \"dotest\",\n      \"tap .doaddcomment\": \"doaddcomment\",\n      \"tap .dosave\": \"doshowsave\",\n      \"tap .doprev\": \"doprev\",\n      \"swipeleft\": \"doprev\",\n      \"tap .donext\": \"donext\",\n      \"swiperight\": \"donext\"\n    },\n    doitem: function(ev) {\n      var self;\n      console.log('do item');\n      self = this;\n      $.mobile.changePage($(\".godialog\", self.el), {\n        transition: 'pop',\n        role: 'dialog',\n        reverse: false,\n        changeHash: false\n      });\n      return false;\n    },\n    dotest: function(ev) {\n      console.log('do test');\n      return false;\n    },\n    doshowsave: function(ev) {\n      $(\".showblock\", this.el).hide();\n      if (this.addshowsaveisup) {\n        $(\".showsave\", this.el).hide();\n        this.addshowsaveisup = false;\n        return;\n      }\n      this.addshowsaveisup = true;\n      $(\".showsave\", this.el).show();\n      return false;\n    },\n    addcomment: function(ev) {\n      $(\".showblock\", this.el).hide();\n      if (this.addcommentisup) {\n        $(\".showaddcomment\", this.el).hide();\n        this.addcommentisup = false;\n        return;\n      }\n      this.addcommentisup = true;\n      $(\".showaddcomment\", this.el).show();\n      if (uzerM.isanon()) {\n        $(\".haveuser\", this.el).hide();\n        $(\".anonuser\", this.el).show();\n      }\n      return false;\n    },\n    doaddcomment: function(ev) {\n      var batchdata, comment, d, incpath, mya, self, xhr, xya,\n        _this = this;\n      self = this;\n      comment = $(\"textarea[name='mycomment']\", this.el).val();\n      if (!comment) {\n        $(\"textarea[name='mycomment']\", this.el).css({\n          'border': '1px solid red'\n        });\n        return false;\n      }\n      d = {\n        comment: comment,\n        username: window.Reap.uzer.username,\n        markname: this.model.get(\"username\"),\n        markId: this.model.get(\"objectId\")\n      };\n      mya = {\n        to: window.Reap.uzer.username,\n        username: window.Reap.uzer.username,\n        action: \"commented\",\n        from: this.model.get(\"username\"),\n        objId: this.model.get(\"objectId\")\n      };\n      incpath = \"/1/classes/Mark/\" + this.model.get(\"objectId\");\n      batchdata = {\n        requests: [\n          {\n            method: 'PUT',\n            path: incpath,\n            body: {\n              numComments: {\n                \"__op\": \"Increment\",\n                \"amount\": 1\n              }\n            }\n          }, {\n            method: 'POST',\n            path: \"/1/classes/MarkComment\",\n            body: d\n          }, {\n            method: 'POST',\n            path: \"/1/classes/Activity\",\n            body: mya\n          }\n        ]\n      };\n      if (this.model.get(\"username\") !== window.Reap.uzer.username) {\n        xya = mya;\n        xya.to = this.model.get(\"username\");\n        batchdata.requests.push({\n          method: 'POST',\n          path: \"/1/classes/Activity\",\n          body: xya\n        });\n      }\n      return xhr = $.ajax({\n        url: 'https://api.parse.com/1/batch/',\n        dataType: 'json',\n        contentType: 'application/json; charset=utf-8',\n        type: 'POST',\n        data: JSON.stringify(batchdata),\n        headers: {\n          'X-Parse-Application-Id': window.global.PARSE_ID,\n          'X-Parse-REST-API-Key': window.global.PARSE_KEY\n        },\n        success: function(data) {\n          var op;\n          console.log(\"AJAX success\");\n          $(\"textarea[name='mycomment']\", _this.el).val(\"\");\n          $(\".showaddcomment\", _this.el).hide();\n          op = new oneComment({\n            model: new Backbone.Model(d),\n            parent: $(\".commentlist\", self.el)\n          });\n          return self.addcommentisup = false;\n        }\n      });\n    },\n    domap: function(ev) {\n      var html, lat, lon;\n      $(\".showblock\", this.el).hide();\n      if (this.mapisup) {\n        $(\".showmap\", this.el).html(\"\").hide();\n        this.mapisup = false;\n        return;\n      }\n      lat = this.model.get(\"location\").latitude;\n      lon = this.model.get(\"location\").longitude;\n      html = \"<iframe src='http://maps.google.com/?q=\" + lat + \",\" + lon + \"&z=18&output=embed', style='border:0px;height:250px;width:100%'></iframe>\";\n      $(\".showmap\", this.el).html(html).show();\n      return this.mapisup = true;\n    },\n    render: function() {\n      var authortext, filter, h, notes, self, shareSubject, shareText, w, xhr,\n        _this = this;\n      self = this;\n      notes = this.model.get(\"notes\");\n      if (this.model.get(\"username\") === window.Reap.uzer.username) {\n        authortext = \"my\";\n      } else {\n        authortext = this.model.get(\"username\") + \"'s\";\n      }\n      shareSubject = \"From \" + authortext + \" SnapShop: \" + this.model.get(\"snapshop\");\n      shareText = \"Check out \" + \" http://getreap.com/p/\" + this.model.get(\"objectId\") + \" in \" + authortext + \" \\\"\" + this.model.get(\"snapshop\") + \"\\\" SnapShop\";\n      if (notes) {\n        notes = notes.substring(0, 60);\n        shareText = shareText + \" re: \" + notes;\n      }\n      window.NativeHost.setShare(shareSubject, shareText);\n      w = (($(window).width()) - 12).toString().split('.')[0];\n      h = (($(window).height()) - 100).toString().split('.')[0];\n      $(this.el).html(this.template({\n        mark: this.model.toJSON(),\n        w: w,\n        h: h\n      }));\n      $(\"#content\").hide();\n      $(\"#overlay\").empty().append(this.el).show();\n      $(this.el).trigger('create');\n      $(\".ui-icon-arrow-r\", $(\".detailbody\", this.el)).remove();\n      filter = {\n        markId: this.model.get(\"objectId\")\n      };\n      return xhr = $.ajax({\n        url: 'https://api.parse.com/1/classes/MarkComment',\n        dataType: 'json',\n        type: 'GET',\n        data: {\n          order: '-createdAt',\n          limit: 10,\n          skip: 0,\n          'where': filter\n        },\n        headers: {\n          'X-Parse-Application-Id': window.global.PARSE_ID,\n          'X-Parse-REST-API-Key': window.global.PARSE_KEY\n        },\n        success: function(data) {\n          var collection;\n          collection = new Backbone.Collection;\n          collection.comparator = function(m) {\n            return m.get(\"createdAt\");\n          };\n          collection.add(data.results);\n          if (!collection.length) {\n            return;\n          } else {\n            console.log('loaded ' + collection.length);\n          }\n          return collection.each(function(cmt) {\n            var op;\n            return op = new oneComment({\n              model: cmt,\n              parent: $(\".commentlist\", self.el)\n            });\n          });\n        }\n      });\n    },\n    doprev: function() {\n      var x;\n      x = 1;\n      return false;\n    },\n    donext: function() {\n      var x;\n      x = 1;\n      return false;\n    }\n  });\n\n}).call(this);\n\n//@ sourceURL=/snappage/detail.coffee"
));

require.define("/snappage/onecomment.coffee",Function(['require','module','exports','__dirname','__filename','process','global'],"(function() {\n  var onecomment_template;\n\n  onecomment_template = require('./onecomment.jade');\n\n  module.exports = Backbone.View.extend({\n    initialize: function() {\n      return this.render();\n    },\n    render: function() {\n      $(this.el).html(onecomment_template({\n        comment: this.model.toJSON()\n      }));\n      $(this.options.parent).append(this.el);\n      $(this.el).trigger('create');\n      $(\".ui-icon-arrow-r\", $(this.el)).remove();\n      return this;\n    }\n  });\n\n}).call(this);\n\n//@ sourceURL=/snappage/onecomment.coffee"
));

require.define("/snappage/onecomment.jade",Function(['require','module','exports','__dirname','__filename','process','global'],"module.exports = function anonymous(locals, attrs, escape, rethrow, merge) {\nattrs = attrs || jade.attrs; escape = escape || jade.escape; rethrow = rethrow || jade.rethrow; merge = merge || jade.merge;\nvar buf = [];\nwith (locals || {}) {\nvar interp;\nbuf.push('<ul data-role=\"listview\" data-inset=\"true\" style=\"margin:5px 10px\"><li><a style=\"padding-right:5px\" class=\"doitem\"><img src=\"comments.png\" style=\"margin:15px;width:50px\"/><h3>' + escape((interp = comment.username) == null ? '' : interp) + '</h3><p>' + escape((interp = comment.comment) == null ? '' : interp) + '</p><p class=\"ui-li-aside\">7:38pm</p></a></li></ul>');\n}\nreturn buf.join(\"\");\n};\n//@ sourceURL=/snappage/onecomment.jade"
));

require.define("/snappage/detail.jade",Function(['require','module','exports','__dirname','__filename','process','global'],"module.exports = function anonymous(locals, attrs, escape, rethrow, merge) {\nattrs = attrs || jade.attrs; escape = escape || jade.escape; rethrow = rethrow || jade.rethrow; merge = merge || jade.merge;\nvar buf = [];\nwith (locals || {}) {\nvar interp;\nbuf.push('<div><div style=\"position:fixed;top:0px;left:0px;width:100%;z-index:9999\"><div class=\"ui-grid-b\"><div style=\"width:10%\" class=\"ui-block-a\"><a data-role=\"button\" data-icon=\"arrow-l\" data-theme=\"a\" data-iconpos=\"notext\" class=\"doprev\"></a></div><div style=\"width:80%\" class=\"ui-block-b\"><div data-role=\"navbar\" data-iconpos=\"right\"><ul><li><a data-icon=\"arrow-d\" data-theme=\"b\" class=\"domap\">Map</a></li><li><a data-icon=\"check\" data-theme=\"b\" class=\"dosave\">Save</a></li><li><a data-icon=\"plus\" data-theme=\"b\" class=\"addcomment\">Comment</a></li></ul></div></div><div style=\"width:10%\" class=\"ui-block-c\"><a style=\"float:right\" data-role=\"button\" data-icon=\"arrow-r\" data-theme=\"a\" data-iconpos=\"notext\" class=\"donext\"></a><br style=\"clear:both\"/></div></div><div style=\"padding:5px 0px;display:none\" class=\"showmap showblock\"></div><div style=\"text-align:center;background-color:#ffffe0;padding:10px;display:none\" class=\"showsave showblock\"><div style=\"margin:20px auto\">Not yet implemented</div></div><div style=\"text-align:center;background-color:#ffffe0;padding:10px;display:none\" class=\"showaddcomment showblock\"><div style=\"margin:10px auto\" class=\"haveuser\"><textarea name=\"mycomment\" style=\"height:60px;width:100%\"></textarea><button data-theme=\"a\" class=\"doaddcomment\">Submit Comment</button></div><div style=\"margin:10px auto;display:none\" class=\"anonuser\"><div style=\"margin:20px auto\">You need to be logged in</div></div></div></div></div><div class=\"detailbody\"><div style=\"margin-top:55px\"><img');\nbuf.push(attrs({ 'src':(\"\" + (mark.imgSrc) + \"\"), 'style':('margin-top:-15px;padding:0px 5px;width:' + (w) + 'px'), \"class\": ('mark') }, {\"src\":true,\"style\":true}));\nbuf.push('/></div><ul data-role=\"listview\" data-inset=\"true\" style=\"margin:10px 10px 10px 10px\"><li><a style=\"padding-right:5px\" class=\"doitem\"><img src=\"comments.png\" style=\"margin:15px;width:50px\"/><h3>' + escape((interp = mark.username) == null ? '' : interp) + '</h3><p class=\"comment-show-all\">' + escape((interp = mark.notes) == null ? '' : interp) + '<span class=\"ui-link pointer dotest\">test</span></p><p class=\"ui-li-aside\">6:42pm</p></a></li></ul><ul data-role=\"listview\" data-inset=\"true\" style=\"margin:5px 10px\"><li data-role=\"list-divider\"> \\nComments');\nif ( mark.numComments)\n{\nbuf.push('<span class=\"ui-li-count\">' + escape((interp = mark.numComments) == null ? '' : interp) + '</span>');\n}\nbuf.push('</li></ul><div class=\"commentlist\"></div><div data-role=\"page\" class=\"godialog\"><div data-role=\"header\"><h1>Hi</h1></div><div data-role=\"content\"><p>Stuff here</p></div></div></div>');\n}\nreturn buf.join(\"\");\n};\n//@ sourceURL=/snappage/detail.jade"
));

require.define("/snappage/onepage.coffee",Function(['require','module','exports','__dirname','__filename','process','global'],"(function() {\n  var onePic, onepage_template;\n\n  onepage_template = require('./onepage.jade');\n\n  onePic = require('./onepic');\n\n  module.exports = Backbone.View.extend({\n    initialize: function() {\n      if (!this.collection) {\n        console.log('no collection');\n        this.collection = new Backbone.Collection();\n      }\n      if (!this.collection.length) {\n        return;\n      }\n      return this.render();\n    },\n    render: function() {\n      var crow, self,\n        _this = this;\n      self = this;\n      crow = this.collection.at(0);\n      $(this.el).html(onepage_template({\n        crow: crow.toJSON(),\n        store: 'blah'\n      }));\n      $(this.options.parent).append(this.el);\n      this.collection.each(function(mark) {\n        var op;\n        return op = new onePic({\n          model: mark,\n          parent: $(\".photogrid\", self.el),\n          container: 'page'\n        });\n      });\n      return this;\n    }\n  });\n\n}).call(this);\n\n//@ sourceURL=/snappage/onepage.coffee"
));

require.define("/snappage/onepage.jade",Function(['require','module','exports','__dirname','__filename','process','global'],"module.exports = function anonymous(locals, attrs, escape, rethrow, merge) {\nattrs = attrs || jade.attrs; escape = escape || jade.escape; rethrow = rethrow || jade.rethrow; merge = merge || jade.merge;\nvar buf = [];\nwith (locals || {}) {\nvar interp;\nbuf.push('<div><div class=\"photogrid\"></div></div>');\n}\nreturn buf.join(\"\");\n};\n//@ sourceURL=/snappage/onepage.jade"
));

require.define("/snappage/explore.jade",Function(['require','module','exports','__dirname','__filename','process','global'],"module.exports = function anonymous(locals, attrs, escape, rethrow, merge) {\nattrs = attrs || jade.attrs; escape = escape || jade.escape; rethrow = rethrow || jade.rethrow; merge = merge || jade.merge;\nvar buf = [];\nwith (locals || {}) {\nvar interp;\nbuf.push('<div class=\"homepanel\"></div>');\n}\nreturn buf.join(\"\");\n};\n//@ sourceURL=/snappage/explore.jade"
));

require.define("/snappage/mysnaps.coffee",Function(['require','module','exports','__dirname','__filename','process','global'],"(function() {\n  var onePage, oneRow;\n\n  oneRow = require('./onerow.coffee');\n\n  onePage = require('./onepage.coffee');\n\n  module.exports = Backbone.View.extend({\n    initialize: function(url) {\n      this.url = url;\n      this.template = require('./mysnaps.jade');\n      this.render();\n      return this;\n    },\n    render: function() {\n      var _this = this;\n      this.$el.html(this.template({\n        url: this.url\n      }));\n      this.length = 0;\n      this.$el.off('infiniteScroll');\n      this.$el.on('infiniteScroll', function() {\n        return _this.load();\n      });\n      return this;\n    },\n    xload: function() {\n      if (this.loading) {\n        return;\n      }\n      this.loading = true;\n      console.debug(\"loading \" + this.url);\n      Parse.Cloud.run('geomarks', {}, {\n        success: function(data) {\n          var ar, collection;\n          collection = new Backbone.Collection(data);\n          if (!collection.length) {\n            return;\n          }\n          ar = new onePage({\n            collection: collection,\n            parent: $(\".homepanel\", this.$el)\n          });\n          /*\n                          ar = new oneRow({collection: collection, parent: $(\".homepanel\", @$el)})\n                          ar = new oneRow({collection: collection, parent: $(\".homepanel\", @$el)})\n                          ar = new oneRow({collection: collection, parent: $(\".homepanel\", @$el)})\n                          ar = new oneRow({collection: collection, parent: $(\".homepanel\", @$el)})\n          */\n\n          this.length += collection.length;\n          return this.loading = false;\n        }\n      });\n      return this;\n    },\n    load: function() {\n      var filter, xhr,\n        _this = this;\n      if (this.loading) {\n        return;\n      }\n      this.loading = true;\n      console.log('h ' + window.location.href.toString());\n      if (window.location.href.toString().indexOf('mysnaps') === -1) {\n        return;\n      }\n      filter = {\n        username: window.Reap.uzer.username\n      };\n      console.debug(\"loading \" + this.length);\n      xhr = $.ajax({\n        url: 'https://api.parse.com/1/classes/Mark',\n        dataType: 'json',\n        headers: {\n          'X-Parse-Application-Id': window.global.PARSE_ID,\n          'X-Parse-REST-API-Key': window.global.PARSE_KEY\n        },\n        data: {\n          order: '-createdAt',\n          limit: 12,\n          skip: this.length,\n          'where': filter\n        },\n        success: function(data) {\n          var ar, collection;\n          console.log(\"AJAX success\");\n          collection = new Backbone.Collection(data.results);\n          if (!collection.length) {\n            return;\n          } else {\n            console.log('loaded ' + collection.length);\n          }\n          ar = new onePage({\n            collection: collection,\n            parent: $(\".homepanel\", _this.$el)\n          });\n          _this.length += collection.length;\n          _this.loading = false;\n          return console.debug(\"length is \" + _this.length);\n        },\n        error: function(jqXHR, textStatus, errorThrown) {\n          return console.error(errorThrown);\n        }\n      });\n      xhr.complete(function() {\n        console.log(\"AJAX complete\");\n        return _this.loading = false;\n      });\n      return this;\n    }\n  });\n\n}).call(this);\n\n//@ sourceURL=/snappage/mysnaps.coffee"
));

require.define("/snappage/mysnaps.jade",Function(['require','module','exports','__dirname','__filename','process','global'],"module.exports = function anonymous(locals, attrs, escape, rethrow, merge) {\nattrs = attrs || jade.attrs; escape = escape || jade.escape; rethrow = rethrow || jade.rethrow; merge = merge || jade.merge;\nvar buf = [];\nwith (locals || {}) {\nvar interp;\nbuf.push('<div class=\"homepanel\"></div>');\n}\nreturn buf.join(\"\");\n};\n//@ sourceURL=/snappage/mysnaps.jade"
));

require.define("/upload/uploadMark.coffee",Function(['require','module','exports','__dirname','__filename','process','global'],"(function() {\n  var uzer;\n\n  uzer = require(\"../models/uzer\");\n\n  module.exports = Backbone.View.extend({\n    initialize: function(imgFileLocation) {\n      this.imgFileLocation = imgFileLocation;\n      this.template = require('./uploadMark.jade');\n      this.render();\n      return this;\n    },\n    events: {\n      \"tap .shownew\": \"shownew\",\n      \"tap .hidenew\": \"hidenew\",\n      \"tap .snapshopsel\": \"dosnapshopsel\"\n    },\n    dosnapshopsel: function() {\n      $(\".snapshoppop\").popup('open', {\n        tolerance: \"30,15,30,15\"\n      });\n      $(\".thesnapshop\").tap(function() {\n        var ssel;\n        ssel = $(this).attr('data-x');\n        $(\".ui-btn-text\", $(\".snapshopsel\")).html(ssel);\n        return $(\".snapshoppop\").popup('close');\n      });\n      return false;\n    },\n    shownew: function() {\n      $(\".selrow\", this.el).hide();\n      $(\".newrow\", this.el).show();\n      return false;\n    },\n    hidenew: function() {\n      $(\".selrow\", this.el).show();\n      $(\".newrow\", this.el).hide();\n      $(\"input[name='newsnapshop']\").val(\"\");\n      return false;\n    },\n    render: function() {\n      var button, snapshops, snapshopsC, w,\n        _this = this;\n      $(\"#overlay\").hide();\n      this.$el.empty();\n      $(\".snapshoppop\").remove();\n      w = (($(window).width() / 3) - 6).toString().split('.')[0];\n      try {\n        snapshops = window.Reap.uzer.snapshops.toJSON();\n      } catch (e) {\n        snapshopsC = uzer.getSnapshops();\n        window.Reap.uzer.snapshops = snapshopsC;\n        snapshops = snapshopsC.toJSON();\n      }\n      this.$el.append(this.template({\n        imgFileLocation: this.imgFileLocation,\n        w: w,\n        snapshops: snapshops\n      }));\n      $(\"#content\").empty().append(this.el).show();\n      $(this.el).trigger('create');\n      button = $('button.btnUpload', this.$el);\n      return button.button().on('tap', function() {\n        return _this.uploadPic(_this.imgFileLocation);\n      });\n    },\n    uploadPic: function(imgFileLocation) {\n      var imgSrc, location, md, notes, snapshop,\n        _this = this;\n      $.mobile.loading('show');\n      $('button.btnUpload').button('disable');\n      location = JSON.parse(NativeHost.getBestLocation());\n      location['__type'] = 'GeoPoint';\n      imgSrc = NativeHost.uploadImage(imgFileLocation);\n      notes = $('#notes').val();\n      snapshop = $(\"input[name='newsnapshop']\").val();\n      if (snapshop) {\n        window.Reap.uzer.snapshops.donew(snapshop);\n      } else {\n        snapshop = $(\"select[name='snapshop']\").val();\n        window.Reap.uzer.snapshops.touch(snapshop);\n      }\n      md = {\n        username: window.Reap.uzer.username,\n        imgSrc: imgSrc,\n        notes: $('#notes').val(),\n        snapshop: snapshop,\n        location: location\n      };\n      return $.ajax({\n        type: 'POST',\n        url: 'https://api.parse.com/1/classes/Mark',\n        dataType: 'json',\n        contentType: 'application/json; charset=utf-8',\n        headers: {\n          'X-Parse-Application-Id': window.global.PARSE_ID,\n          'X-Parse-REST-API-Key': window.global.PARSE_KEY\n        },\n        data: JSON.stringify(md),\n        success: function(data) {\n          NativeHost.toast(\"Image Uploaded!\");\n          return window.Reap.navigate('mysnaps');\n        },\n        error: function(jqXHR, textStatus, errorThrown) {\n          NativeHost.toast(\"POST failed: \" + errorThrown);\n          console.error(errorThrown);\n          return console.error(JSON.stringify(jqXHR));\n        },\n        complete: function() {\n          $.mobile.loading('hide');\n          return window.Reap.navigate('mysnaps');\n        }\n      });\n    }\n  });\n\n}).call(this);\n\n//@ sourceURL=/upload/uploadMark.coffee"
));

require.define("/settings/settings.coffee",Function(['require','module','exports','__dirname','__filename','process','global'],"(function() {\n\n  module.exports = Backbone.View.extend({\n    initialize: function(url) {\n      this.url = url;\n      this.template = require('./settings.jade');\n      this.render();\n      return this;\n    },\n    render: function() {\n      this.$el.html(this.template);\n      $('#content').empty().append(this.$el);\n      $(this.el).trigger('create');\n      return this;\n    }\n  });\n\n}).call(this);\n\n//@ sourceURL=/settings/settings.coffee"
));

require.define("/settings/settings.jade",Function(['require','module','exports','__dirname','__filename','process','global'],"module.exports = function anonymous(locals, attrs, escape, rethrow, merge) {\nattrs = attrs || jade.attrs; escape = escape || jade.escape; rethrow = rethrow || jade.rethrow; merge = merge || jade.merge;\nvar buf = [];\nwith (locals || {}) {\nvar interp;\nbuf.push('<div style=\"margin:20px\"><ul data-role=\"listview\" data-inset=\"true\"><li><a href=\"#account\">Account</a></li><li><a href=\"#notifcations\">Notifications</a></li></ul></div>');\n}\nreturn buf.join(\"\");\n};\n//@ sourceURL=/settings/settings.jade"
));

require.define("/account/account.coffee",Function(['require','module','exports','__dirname','__filename','process','global'],"(function() {\n  var accountT, loginT, registerT, uzer;\n\n  accountT = require('./account.jade');\n\n  loginT = require('./login.jade');\n\n  registerT = require('./register.jade');\n\n  uzer = require('../models/uzer');\n\n  module.exports = Backbone.View.extend({\n    initialize: function() {\n      this.render();\n      return this;\n    },\n    events: {\n      \"tap .showLogin\": \"showLogin\",\n      \"tap .showRegister\": \"showRegister\",\n      \"tap .doLogout\": \"doLogout\",\n      \"tap .doLogin\": \"doLogin\",\n      \"tap .doRegister\": \"doRegister\"\n    },\n    render: function() {\n      if (uzer.isanon()) {\n        return this.showLogin();\n      } else {\n        return this.showAccount();\n      }\n    },\n    showAccount: function() {\n      this.$el.html(accountT({\n        uzer: window.Reap.uzer\n      }));\n      $('#content').empty().append(this.$el);\n      $(this.el).trigger('create');\n      this.delegateEvents();\n      return this;\n    },\n    showLogin: function() {\n      this.$el.html(loginT);\n      $('#content').empty().append(this.$el);\n      $(this.el).trigger('create');\n      this.delegateEvents();\n      return false;\n    },\n    doLogin: function() {\n      var password, self, username;\n      self = this;\n      username = $(\"input[name='username']\", this.el).val();\n      password = $(\"input[name='password']\", this.el).val();\n      uzer.login(username, password, function(err) {\n        if (err) {\n          $(\".messagespot\", this.el).html(err);\n        } else {\n          self.showAccount();\n        }\n        return false;\n      });\n      return false;\n    },\n    showRegister: function() {\n      this.$el.html(registerT);\n      $('#content').empty().append(this.$el);\n      $(this.el).trigger('create');\n      this.delegateEvents();\n      return false;\n    },\n    xdoRegister: function() {\n      var self;\n      self = this;\n      return false;\n    },\n    doLogout: function() {\n      var self;\n      self = this;\n      return uzer.logout(function() {\n        return self.showLogin();\n      });\n    },\n    doRegister: function() {\n      var email, html, password, self, username;\n      self = this;\n      username = $(\"input[name='username']\", this.el).val();\n      password = $(\"input[name='password']\", this.el).val();\n      email = $(\"input[name='email']\").val();\n      $(\".messagespot\", this.el).html(\"\");\n      if (!uzer.validateEmail(email)) {\n        html = \"Invalid email\";\n        $(\".messagespot\", this.el).html(html);\n        return false;\n      }\n      if (!uzer.validateUsername(username)) {\n        html = \"Invalid username\";\n        $(\".messagespot\", this.el).html(html);\n        return false;\n      }\n      if (!uzer.validatePassword(password)) {\n        html = \"Invalid password\";\n        $(\".messagespot\", this.el).html(html);\n        return false;\n      }\n      uzer.signup(username, password, email, function(err) {\n        if (err) {\n          $(\".messagespot\", self.el).html(err);\n        } else {\n          self.showAccount();\n        }\n        return false;\n      });\n      return false;\n    }\n  });\n\n}).call(this);\n\n//@ sourceURL=/account/account.coffee"
));

require.define("/account/account.jade",Function(['require','module','exports','__dirname','__filename','process','global'],"module.exports = function anonymous(locals, attrs, escape, rethrow, merge) {\nattrs = attrs || jade.attrs; escape = escape || jade.escape; rethrow = rethrow || jade.rethrow; merge = merge || jade.merge;\nvar buf = [];\nwith (locals || {}) {\nvar interp;\nbuf.push('<div style=\"margin:20px\"><h3>Account details</h3><div>' + escape((interp = uzer.username) == null ? '' : interp) + '</div><div>' + escape((interp = uzer.email) == null ? '' : interp) + '</div><br/><br/><span class=\"ui-link pointer doLogout\">Logout</span></div>');\n}\nreturn buf.join(\"\");\n};\n//@ sourceURL=/account/account.jade"
));

require.define("/account/login.jade",Function(['require','module','exports','__dirname','__filename','process','global'],"module.exports = function anonymous(locals, attrs, escape, rethrow, merge) {\nattrs = attrs || jade.attrs; escape = escape || jade.escape; rethrow = rethrow || jade.rethrow; merge = merge || jade.merge;\nvar buf = [];\nwith (locals || {}) {\nvar interp;\nbuf.push('<div style=\"margin:20px 40px\"><div style=\"height:40px;margin-bottom:15px;color:red\" class=\"messagespot\">Please Login</div><div><input type=\"text\" name=\"username\" placeholder=\"username\"/><input type=\"password\" name=\"password\" placeholder=\"password\"/><br/><button data-icon=\"arrow-r\" data-iconpos=\"right\" data-theme=\"b\" class=\"doLogin\">Login</button><br/><div style=\"text-align:center\"><a class=\"doforgot\">Forgot Password?</a></div><br/><br/><div>Don\\'t have a Reap account?<br/><span class=\"showRegister ui-link pointer\">Register here</span></div></div></div>');\n}\nreturn buf.join(\"\");\n};\n//@ sourceURL=/account/login.jade"
));

require.define("/account/register.jade",Function(['require','module','exports','__dirname','__filename','process','global'],"module.exports = function anonymous(locals, attrs, escape, rethrow, merge) {\nattrs = attrs || jade.attrs; escape = escape || jade.escape; rethrow = rethrow || jade.rethrow; merge = merge || jade.merge;\nvar buf = [];\nwith (locals || {}) {\nvar interp;\nbuf.push('<div style=\"margin:20px 40px\"><div style=\"height:40px;margin-bottom:15px;color:red\" class=\"messagespot\"></div><div><input type=\"text\" name=\"email\" placeholder=\"email\"/><input type=\"text\" name=\"username\" placeholder=\"username\"/><input type=\"password\" name=\"password\" placeholder=\"password\"/><br/><button data-icon=\"arrow-r\" data-iconpos=\"right\" data-theme=\"b\" class=\"doRegister\">Register</button><br/><br/><div>Already have a Reap account?<br/><span class=\"showLogin ui-link pointer\">Login here</span></div></div></div>');\n}\nreturn buf.join(\"\");\n};\n//@ sourceURL=/account/register.jade"
));

require.define("/account/about.coffee",Function(['require','module','exports','__dirname','__filename','process','global'],"(function() {\n  var aboutT;\n\n  aboutT = require('./about.jade');\n\n  module.exports = Backbone.View.extend({\n    initialize: function() {\n      this.render();\n      return this;\n    },\n    render: function() {\n      this.$el.html(aboutT);\n      $('#content').empty().append(this.$el);\n      $(this.el).trigger('create');\n      this.delegateEvents();\n      return this;\n    }\n  });\n\n}).call(this);\n\n//@ sourceURL=/account/about.coffee"
));

require.define("/account/about.jade",Function(['require','module','exports','__dirname','__filename','process','global'],"module.exports = function anonymous(locals, attrs, escape, rethrow, merge) {\nattrs = attrs || jade.attrs; escape = escape || jade.escape; rethrow = rethrow || jade.rethrow; merge = merge || jade.merge;\nvar buf = [];\nwith (locals || {}) {\nvar interp;\nbuf.push('<div><ul data-role=\"listview\" data-inset=\"true\" style=\"margin:5px 10px\"><li><a style=\"padding-right:5px\" class=\"doitem\"><img src=\"128x128.png\" style=\"margin:15px;width:50px\"/><h3>About Reap</h3><p class=\"comment-show-all\"> \\nSnap Shopping helps you become a smarter shopper.</p><p class=\"ui-li-aside\">getreap.com</p></a></li></ul><div data-role=\"collapsible-set\" data-content-theme=\"c\" style=\"margin:40px 30px\"><div data-role=\"collapsible\" data-theme=\"b\" data-content-theme=\"b\"><h3>Pictures as bookmarks</h3><p>Never forget what that antique looked like!</p></div><div data-role=\"collapsible\" data-theme=\"e\" data-content-theme=\"e\"><h3>Share with family & friends</h3><p>Get all their comments in real-time on the same page</p></div><div data-role=\"collapsible\" data-theme=\"b\" data-content-theme=\"b\"><h3>Discover whats around you<h4>Find what\\'s new and interesting.</h4></h3><ul><li>Browse snap shops of merchants around you </li><li>Browse what other shoppers are snap shopping for.</li></ul></div><div data-role=\"collapsible\" data-theme=\"e\" data-content-theme=\"e\"><h3>Snap Shops</h3><p>Retailers, experts and other shoppers create Snap Shops  that contain 10 to 20 items tha are related, so you can browse them.</p></div><div data-role=\"collapsible\" data-theme=\"b\" data-content-theme=\"b\"><h3>Merchant bids</h3><p>Having expressed your intent through snap shopping you are now on your way to getting the attention of merchants so they can bid for your business in real time.</p></div></div></div>');\n}\nreturn buf.join(\"\");\n};\n//@ sourceURL=/account/about.jade"
));

require.define("/activity/activity.coffee",Function(['require','module','exports','__dirname','__filename','process','global'],"(function() {\n  var oneActivity, template;\n\n  template = require('./activity.jade');\n\n  oneActivity = require('./oneactivity');\n\n  module.exports = Backbone.View.extend({\n    initialize: function() {\n      return this.render();\n    },\n    events: {\n      \"tap .doitem\": \"doitem\"\n    },\n    doitem: function(e) {\n      NativeHost.toast('Display message payload.');\n      return false;\n    },\n    render: function() {\n      var filter, self, xhr,\n        _this = this;\n      self = this;\n      this.$el.html(template);\n      if (window.Reap.uzer.username.indexOf(\"xnov-\") === -1) {\n        $(\".activitylist\", this.el).html(\"\");\n        $('#content').empty().append(this.$el);\n      } else {\n        $('#content').empty().append(this.$el);\n        $(this.el).trigger('create');\n      }\n      filter = {\n        toUsername: window.Reap.uzer.username\n      };\n      return xhr = $.ajax({\n        url: 'https://api.parse.com/1/classes/Activity',\n        dataType: 'json',\n        headers: {\n          'X-Parse-Application-Id': window.global.PARSE_ID,\n          'X-Parse-REST-API-Key': window.global.PARSE_KEY\n        },\n        data: {\n          order: '-createdAt',\n          limit: 20,\n          skip: this.length,\n          'where': filter\n        },\n        success: function(data) {\n          var collection;\n          console.log('d ' + JSON.stringify(data));\n          collection = new Backbone.Collection(data.results);\n          if (!collection.length) {\n            return;\n          } else {\n            console.log('loaded ' + collection.length);\n          }\n          return collection.each(function(anActivity) {\n            var op;\n            return op = new oneActivity({\n              model: anActivity,\n              parent: $(\".activitylist\", self.el)\n            });\n          });\n        },\n        error: function(jqXHR, textStatus, errorThrown) {\n          return console.error(errorThrown);\n        }\n      });\n    }\n  });\n\n}).call(this);\n\n//@ sourceURL=/activity/activity.coffee"
));

require.define("/activity/activity.jade",Function(['require','module','exports','__dirname','__filename','process','global'],"module.exports = function anonymous(locals, attrs, escape, rethrow, merge) {\nattrs = attrs || jade.attrs; escape = escape || jade.escape; rethrow = rethrow || jade.rethrow; merge = merge || jade.merge;\nvar buf = [];\nwith (locals || {}) {\nvar interp;\nbuf.push('<div class=\"activitylist\"><ul data-role=\"listview\" data-inset=\"true\"><li><a class=\"doitem\"><img src=\"128x128.png\" style=\"margin:10px;width:60px\"/><h3>Reap</h3><p>Message: Creating an account.</p><p class=\"ui-li-aside\">6:42pm</p></a></li><li><a class=\"doitem\"><img src=\"128x128.png\" style=\"margin:10px;width:60px\"/><h3>Reap</h3><p>Message: Getting started.</p><p class=\"ui-li-aside\">7:38pm</p></a></li><li><a class=\"doitem\"><img src=\"128x128.png\" style=\"margin:10px;width:60px\"/><h3>Reap</h3><p>Message: Welcome to Reap.</p><p class=\"ui-li-aside\">7:38pm</p></a></li></ul></div>');\n}\nreturn buf.join(\"\");\n};\n//@ sourceURL=/activity/activity.jade"
));

require.define("/activity/oneactivity.coffee",Function(['require','module','exports','__dirname','__filename','process','global'],"(function() {\n  var oneactivity_template;\n\n  oneactivity_template = require('./oneactivity.jade');\n\n  module.exports = Backbone.View.extend({\n    initialize: function() {\n      return this.render();\n    },\n    render: function() {\n      var ago, self;\n      self = this;\n      ago = $.timeago(self.model.get(\"createdAt\"));\n      console.log('ago ' + ago);\n      console.log('aco ' + self.model.get(\"createdAt\"));\n      $(this.el).html(oneactivity_template({\n        activity: this.model.toJSON(),\n        ago: ago\n      }));\n      $(this.options.parent).append(this.el);\n      $(this.el).trigger('create');\n      return this;\n    }\n  });\n\n}).call(this);\n\n//@ sourceURL=/activity/oneactivity.coffee"
));

require.define("/activity/oneactivity.jade",Function(['require','module','exports','__dirname','__filename','process','global'],"module.exports = function anonymous(locals, attrs, escape, rethrow, merge) {\nattrs = attrs || jade.attrs; escape = escape || jade.escape; rethrow = rethrow || jade.rethrow; merge = merge || jade.merge;\nvar buf = [];\nwith (locals || {}) {\nvar interp;\nbuf.push('<ul data-role=\"listview\" data-inset=\"true\"><li><a class=\"doitem\"><img src=\"128x128.png\" style=\"margin:10px;width:60px\"/><h3>' + escape((interp = activity.fromUsername) == null ? '' : interp) + '</h3><p>' + escape((interp = activity.message) == null ? '' : interp) + '</p><p class=\"ui-li-aside\"> ' + escape((interp = ago) == null ? '' : interp) + '</p></a></li></ul>');\n}\nreturn buf.join(\"\");\n};\n//@ sourceURL=/activity/oneactivity.jade"
));

require.define("/sys/nativeHostIos.coffee",Function(['require','module','exports','__dirname','__filename','process','global'],"(function() {\n\n  module.exports = {\n    NativeHost: {\n      selectTab: function(i) {\n        var iframe;\n        iframe = document.createElement(\"IFRAME\");\n        iframe.setAttribute(\"src\", \"js-frame:\" + 'selectTab:' + encodeURIComponent(JSON.stringify(i)));\n        document.documentElement.appendChild(iframe);\n        iframe.parentNode.removeChild(iframe);\n        return iframe = null;\n      },\n      toast: function(message) {\n        var iframe;\n        iframe = document.createElement(\"IFRAME\");\n        iframe.setAttribute(\"src\", \"js-frame:\" + 'Toast:' + encodeURIComponent(JSON.stringify(message)));\n        document.documentElement.appendChild(iframe);\n        iframe.parentNode.removeChild(iframe);\n        return iframe = null;\n      },\n      getEmailHash: function() {\n        var iframe;\n        iframe = document.createElement(\"IFRAME\");\n        iframe.setAttribute(\"src\", \"js-frame:\" + 'getEmailHash:');\n        document.documentElement.appendChild(iframe);\n        iframe.parentNode.removeChild(iframe);\n        return iframe = null;\n      },\n      setEmailHash: function(emailHash) {\n        return JSON.stringify(emailHash);\n      },\n      setShare: function(shareText) {\n        return console.log('set share ' + shareText);\n      },\n      setPushChannel: function(channel) {\n        return console.log('set pushChannel ' + channel);\n      },\n      getBestLocation: function() {\n        /*\n                    dummyLocation =\n                        latitude: 37\n                        longitude: -122\n                        time: Date.now()\n                        accuracy: 10\n        */\n\n        var iframe;\n        iframe = document.createElement(\"IFRAME\");\n        iframe.setAttribute(\"src\", \"js-frame:\" + 'getBestLocation:');\n        document.documentElement.appendChild(iframe);\n        iframe.parentNode.removeChild(iframe);\n        return iframe = null;\n      },\n      setBestLocation: function(userLocation) {\n        return JSON.stringify(userLocation);\n      },\n      uploadImage: function(location) {\n        var iframe;\n        iframe = document.createElement(\"IFRAME\");\n        iframe.setAttribute(\"src\", \"js-frame:\" + 'uploadImage:' + encodeURIComponent(JSON.stringify(location)));\n        document.documentElement.appendChild(iframe);\n        iframe.parentNode.removeChild(iframe);\n        return iframe = null;\n      }\n    }\n  };\n\n}).call(this);\n\n//@ sourceURL=/sys/nativeHostIos.coffee"
));

require.define("/sys/nativeHostDummy.coffee",Function(['require','module','exports','__dirname','__filename','process','global'],"(function() {\n\n  module.exports = {\n    NativeHost: {\n      selectTab: function(i) {\n        return console.log(\"Dummy selectedTab: \" + i);\n      },\n      toast: function(message) {\n        return console.log(\"Dummy toast: \" + message);\n      },\n      getEmailHash: function() {\n        var r;\n        r = Math.floor(10000 + Math.random() * 990000);\n        return r.toString();\n      },\n      setShare: function(shareText) {\n        return console.log('set share ' + shareText);\n      },\n      setPushChannel: function(channel) {\n        return console.log('set pushChannel ' + channel);\n      },\n      getBestLocation: function() {\n        var dummyLocation;\n        console.log(\"Dummy getBestLocation, displaying captured image\");\n        dummyLocation = {\n          latitude: 37,\n          longitude: -122\n        };\n        return JSON.stringify(dummyLocation);\n      },\n      triggerCapture: function() {\n        var imageURL;\n        console.log(\"Dummy triggerCapture, displaying captured image\");\n        imageURL = \"http://reapshots.s3.amazonaws.com/4a7efea3b43b66a0-1351896575847.jpg\";\n        return window.Reap.displayCapturedImage(imageURL);\n      },\n      uploadImage: function(location) {\n        console.log(\"Dummy uploadImage: \" + location);\n        return location;\n      }\n    }\n  };\n\n}).call(this);\n\n//@ sourceURL=/sys/nativeHostDummy.coffee"
));

require.define("/upload/uploadMark.jade",Function(['require','module','exports','__dirname','__filename','process','global'],"module.exports = function anonymous(locals, attrs, escape, rethrow, merge) {\nattrs = attrs || jade.attrs; escape = escape || jade.escape; rethrow = rethrow || jade.rethrow; merge = merge || jade.merge;\nvar buf = [];\nwith (locals || {}) {\nvar interp;\nbuf.push('<form action=\"\" onsubmit=\"return false;\"><button data-theme=\"b\" class=\"btnUpload\">Reap it</button><div class=\"ui-grid-a selrow\"><div style=\"width:70%\" class=\"ui-block-a\"><a data-rel=\"popup\" data-role=\"button\" data-icon=\"arrow-d\" data-iconpos=\"right\" class=\"snapshopsel\">' + escape((interp = snapshops[0].name) == null ? '' : interp) + '</a><div data-role=\"popup\" class=\"snapshoppop\"><div style=\"max-height:400px;overflow:scroll\"><ul data-role=\"listview\" data-inset=\"true\" data-theme=\"a\">');\n// iterate snapshops\n;(function(){\n  if ('number' == typeof snapshops.length) {\n\n    for (var $index = 0, $$l = snapshops.length; $index < $$l; $index++) {\n      var ss = snapshops[$index];\n\nbuf.push('<li><a');\nbuf.push(attrs({ 'data-x':(\"\" + (ss.name) + \"\"), \"class\": ('thesnapshop') }, {\"data-x\":true}));\nbuf.push('>' + escape((interp = ss.name) == null ? '' : interp) + '</a></li>');\n    }\n\n  } else {\n    var $$l = 0;\n    for (var $index in snapshops) {\n      $$l++;      var ss = snapshops[$index];\n\nbuf.push('<li><a');\nbuf.push(attrs({ 'data-x':(\"\" + (ss.name) + \"\"), \"class\": ('thesnapshop') }, {\"data-x\":true}));\nbuf.push('>' + escape((interp = ss.name) == null ? '' : interp) + '</a></li>');\n    }\n\n  }\n}).call(this);\n\nbuf.push('</ul></div></div></div><div style=\"width:30%\" class=\"ui-block-b\"><button data-icon=\"plus\" data-iconpos=\"right\" class=\"shownew\">new</button></div></div><div style=\"margin:0px 10px;display:none\" class=\"newrow\"><div style=\"width:70%\" class=\"ui-block-a\"><input type=\"text\" name=\"newsnapshop\" placeholder=\"new snapshop\"/></div><div style=\"width:20%;margin:0px 0px 0px 15px\" class=\"ui-block-b\"><button data-icon=\"minus\" data-iconpos=\"notext\" class=\"hidenew\"></button></div></div><div style=\"margin:0px 10px\"><textarea id=\"notes\" style=\"height:80px;\" placeholder=\"Add #notes (optional)\"></textarea></div><br/><div style=\"text-align:center;padding:5px\"><img');\nbuf.push(attrs({ 'src':(\"\" + (imgFileLocation) + \"\"), 'style':('width:95%') }, {\"src\":true,\"style\":true}));\nbuf.push('/></div></form>');\n}\nreturn buf.join(\"\");\n};\n//@ sourceURL=/upload/uploadMark.jade"
));

require.define("/index.coffee",Function(['require','module','exports','__dirname','__filename','process','global'],"(function() {\n  var AppRouter, TAB_ACTIVITY, TAB_CAMERA, TAB_EXPLORE, TAB_MYSNAPS, TAB_SETTINGS, aboutM, accountM, activityM, appRouter, detailM, exploreM, global, mysnapsM, settingsM, uploadM, util, uzer;\n\n  util = require('./util');\n\n  uzer = require('./models/uzer');\n\n  global = window.global;\n\n  window.global.API_URL = \"http://att.getreap.com:1451/\";\n\n  window.global.PARSE_ID = \"RT4rZZGWBCJ0u0gZicHXSPGWASy80839LL3844bB\";\n\n  window.global.PARSE_KEY = \"q5POwx2KiZS8HD4TX85oyargNBjZQ0s8cbA5SwDu\";\n\n  window.global.PARSE_JS_KEY = \"wPrZv68USEwocxHiLB4F5JgrpUMPCAi4sRIpU0no\";\n\n  $.mobile.hashListeningEnabled = false;\n\n  $.mobile.linkBindingEnabled = false;\n\n  $.mobile.pushStateEnabled = false;\n\n  $.mobile.ajaxEnabled = false;\n\n  TAB_ACTIVITY = 0;\n\n  TAB_EXPLORE = 1;\n\n  TAB_CAMERA = 2;\n\n  TAB_MYSNAPS = 3;\n\n  TAB_SETTINGS = 4;\n\n  exploreM = require('./snappage/explore');\n\n  mysnapsM = require('./snappage/mysnaps');\n\n  uploadM = require('./upload/uploadMark');\n\n  settingsM = require('./settings/settings');\n\n  accountM = require('./account/account');\n\n  aboutM = require('./account/about');\n\n  activityM = require('./activity/activity');\n\n  detailM = require('./snappage/detail');\n\n  Parse.initialize(window.global.PARSE_ID, window.global.PARSE_JS_KEY);\n\n  AppRouter = Backbone.Router.extend({\n    routes: {\n      '': 'activity',\n      'activity': 'activity',\n      'explore': 'explore',\n      'mysnaps': 'mysnaps',\n      'settings': 'settings',\n      'account': 'account',\n      'upload': 'upload',\n      'about': 'about',\n      'details/:id': 'details',\n      'p/:id': 'getDetails',\n      '*path': 'notFound'\n    },\n    explore: function() {\n      var exploreV;\n      NativeHost.selectTab(TAB_EXPLORE);\n      $('#content').show();\n      $('#overlay').hide();\n      exploreV = new exploreM();\n      global.currentView = exploreV;\n      exploreV.render();\n      exploreV.load();\n      return $('#content').empty().append(exploreV.el);\n    },\n    mysnaps: function() {\n      var mysnapsV;\n      if (uzer.isanon()) {\n        window.Reap.navigate(\"#account\", true);\n        return;\n      }\n      NativeHost.selectTab(TAB_MYSNAPS);\n      $('#content').show();\n      $('#overlay').hide();\n      mysnapsV = new mysnapsM();\n      global.currentView = mysnapsV;\n      mysnapsV.render();\n      mysnapsV.load();\n      return $('#content').empty().append(mysnapsV.el);\n    },\n    settings: function() {\n      var settingsV;\n      NativeHost.selectTab(TAB_SETTINGS);\n      $('#content').show();\n      $('#overlay').hide();\n      settingsV = new settingsM();\n      return global.currentView = settingsV;\n    },\n    activity: function() {\n      var activityV;\n      NativeHost.selectTab(TAB_ACTIVITY);\n      $('#content').show();\n      $('#overlay').hide();\n      activityV = new activityM();\n      return global.currentView = activityV;\n    },\n    account: function() {\n      var accountV;\n      NativeHost.selectTab(TAB_SETTINGS);\n      $('#content').show();\n      $('#overlay').hide();\n      accountV = new accountM();\n      return global.currentView = accountV;\n    },\n    about: function() {\n      var aboutV;\n      NativeHost.selectTab(TAB_SETTINGS);\n      $('#content').show();\n      $('#overlay').hide();\n      aboutV = new aboutM();\n      return global.currentView = aboutV;\n    },\n    upload: function() {\n      var uploadMarkView;\n      if (uzer.isanon()) {\n        window.Reap.navigate(\"#account\", true);\n        return;\n      }\n      if (!window.global.fileUploadUrl) {\n        window.global.fileUploadUrl = \"http://reapshots.s3.amazonaws.com/4a7efea3b43b66a0-1352163701374.jpg\";\n      }\n      console.log(\"Upload: \" + window.global.fileUploadUrl);\n      uploadMarkView = new uploadM(window.global.fileUploadUrl);\n      return global.currentView = uploadMarkView;\n    },\n    details: function(id) {\n      $(\"#content\").hide();\n      return $(\"#overlay\").show();\n    },\n    getDetails: function(id) {\n      var detailV;\n      $('#content').show();\n      $('#overlay').hide();\n      detailV = new detailM();\n      global.currentView = detailV;\n      return detailV.render();\n    },\n    notFound: function(path) {\n      NativeHost.toast(\"Error: invalid path \" + path);\n      return console.error(\"Not Found: \" + path);\n    }\n  });\n\n  appRouter = new AppRouter();\n\n  $(function() {\n    var nativeHostDummy, nativeHostIos;\n    if (window.NativeHost) {\n      return console.log('got NativeHost');\n    } else {\n      if (window.Reap.hostType === 'ios') {\n        console.log('initializing NativeHostIos');\n        nativeHostIos = require('./sys/nativeHostIos');\n        return window.NativeHost = nativeHostIos.NativeHost;\n      } else {\n        console.log('initializing NativeHostDummy');\n        nativeHostDummy = require('./sys/nativeHostDummy');\n        return window.NativeHost = nativeHostDummy.NativeHost;\n      }\n    }\n  });\n\n  window.Reap = {\n    navigate: function(path, trigger) {\n      if (trigger == null) {\n        trigger = true;\n      }\n      return appRouter.navigate(path, {\n        trigger: trigger\n      });\n    },\n    displayCapturedImage: function(url) {\n      window.global.fileUploadUrl = url;\n      return appRouter.navigate('upload', {\n        trigger: true\n      });\n    },\n    setHost: function(hostType) {\n      return window.Reap.hostType = hostType;\n    }\n  };\n\n  $(function() {\n    var $body, $document, SCROLL_MARGIN, windowHeight,\n      _this = this;\n    SCROLL_MARGIN = 150;\n    windowHeight = $(window).height();\n    $document = $(document);\n    $body = $(document.body);\n    $(window).scroll(function() {\n      if (!global.currentView) {\n        return;\n      }\n      if ((windowHeight + SCROLL_MARGIN) > $document.height()) {\n        if ((windowHeight + $body.scrollTop()) >= $document.height()) {\n          global.currentView.$el.trigger('infiniteScroll');\n        }\n        return;\n      }\n      if (($body.scrollTop() + windowHeight + SCROLL_MARGIN) > $document.height()) {\n        return global.currentView.$el.trigger('infiniteScroll');\n      }\n    });\n    $(\"a\").click(function(ev) {\n      var href;\n      console.log('y');\n      href = $(this).attr('href');\n      if (href) {\n        if (href[0] === '#') {\n          window.Reap.navigate(href, true);\n        }\n      } else {\n        return true;\n      }\n      return false;\n    });\n    $(\"form\").submit(function() {\n      console.log('in form');\n      return false;\n    });\n    uzer.initialize();\n    return Backbone.history.start();\n  });\n\n}).call(this);\n\n//@ sourceURL=/index.coffee"
));
require("/index.coffee");
})();
